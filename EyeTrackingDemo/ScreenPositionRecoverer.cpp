#include "ScreenPositionRecoverer.h"
#include <MouseHooker.h>

static int s_height = 1080;
static int s_width = 1920;

ScreenPositionRecoverer::ScreenPositionRecoverer()
{
	auto sizeOfVectors = MouseListener::getInstance()->MonitorCount() * 3;
	m_variables.resize(sizeOfVectors);
	m_points.resize(sizeOfVectors);
	m_unknownVariables.resize(sizeOfVectors);
	
}
#ifndef SINGLE_SCREEN

void ScreenPositionRecoverer::initialize(EyeFeatureToPointMappingVector eyeFeayuresAndPointsVector)
{	
	for (auto i = 0; i < eyeFeayuresAndPointsVector.size(); ++i) {
		for (auto j = 0; j < 3; ++j) {
			addVariables(eyeFeayuresAndPointsVector.at(i), -1, i, j);
		}
		continue;
		for (auto const& f : eyeFeayuresAndPointsVector.at(i)) {
			//for (auto j = 0; j < 3; ++j) {
			//	addVariables(f, -1, i, j);
			//}
			//KnownVariables currentKnownVariables;
			//auto feature = f.first.getFeature();
			//auto normalizedCoordinates = getNormalizedScreenCoordinates(f.second);

			////Head position
			//currentKnownVariables.headPositionX = feature.at(0) / 1000;
			//currentKnownVariables.headPositionY = feature.at(1) / 1000;
			//currentKnownVariables.headPositionZ = feature.at(2) / 1000;

			////Head forward vector
			//auto modF = std::sqrt(feature.at(3) * feature.at(3) + feature.at(4) * feature.at(4) + feature.at(5) * feature.at(5));
			//currentKnownVariables.headForwardX = feature.at(3) / modF;
			//currentKnownVariables.headForwardY = feature.at(4) / modF;
			//currentKnownVariables.headForwardZ = feature.at(5) / modF;
			//std::cout << "Forward Z " << currentKnownVariables.headForwardZ << std::endl;
			////Screen normalaized coordinates
			//currentKnownVariables.coordinateX = normalizedCoordinates.first;
			//currentKnownVariables.coordinateY = normalizedCoordinates.second;

			//m_variables.at(i).push_back(currentKnownVariables);// = UnknownVariables();
			//m_points.at(i).push_back(f.second);
		}
	}
	//for (auto i = 0; i < m_variables.size(); ++i) {
	//	std::cout << " det: " << i << " m_variables.at(i).size(): " << m_variables.at(i).size() << std::endl;
	//}
}
#else

void ScreenPositionRecoverer::initialize(EyeFeatureToPointMapping eyeFeayuresAndPoints)
{
	std::cout << "EFAP : " << eyeFeayuresAndPoints.size() << std::endl;
	for (auto const & f : eyeFeayuresAndPoints) {
		
		KnownVariables currentKnownVariables;
		auto feature = f.first.getFeature();
		auto normalizedCoordinates = getNormalizedScreenCoordinates(f.second);

		//Head position
		currentKnownVariables.headPositionX = feature.at(0)/1000;
		currentKnownVariables.headPositionY = feature.at(1)/1000;
		currentKnownVariables.headPositionZ = feature.at(2)/1000;

		//Head forward vector
		auto modF = std::sqrt(feature.at(3)*feature.at(3) + feature.at(4)*feature.at(4) + feature.at(5)*feature.at(5));
		currentKnownVariables.headForwardX = feature.at(3)/modF;
		currentKnownVariables.headForwardY = feature.at(4)/modF;
		currentKnownVariables.headForwardZ = feature.at(5)/modF;
		std::cout << "Forward Z " << currentKnownVariables.headForwardZ << std::endl;
		//Screen normalaized coordinates
		currentKnownVariables.coordinateX = normalizedCoordinates.first;
		currentKnownVariables.coordinateY = normalizedCoordinates.second;

		m_variables.push_back(currentKnownVariables);// = UnknownVariables();
		m_points.push_back(f.second);
	}
}
#endif // !SINGLE_SCREEN

int ScreenPositionRecoverer::getScreensCount()
{
	return m_variables.size();
}

cv::Mat ScreenPositionRecoverer::solve(int indexOfScreen)
{
	if (m_variables.at(indexOfScreen).size() < 15) {
		std::cout << " Screen number " << indexOfScreen << " is not ready for detection " << std::endl;
		return cv::Mat();
	}
	cv::Mat _mat;
	createMat(_mat,indexOfScreen);
	cv::Mat _matTransposed = _mat.t();
	cv::Mat equalTo = matEqualTo(indexOfScreen);

	auto invMat = (_mat.t() * _mat).inv();

	cv::Mat dst = (_mat.t() * _mat).inv() * _mat.t() * equalTo;

	auto dstCol = dst.col(0);
	m_unknownVariables.at(indexOfScreen).screenPositionX = dstCol.at<float>(0);
	m_unknownVariables.at(indexOfScreen).screenPositionY = dstCol.at<float>(1);
	m_unknownVariables.at(indexOfScreen).screenPositionZ = dstCol.at<float>(2);

	m_unknownVariables.at(indexOfScreen).screenLeftX = dstCol.at<float>(3);
	m_unknownVariables.at(indexOfScreen).screenLeftY = dstCol.at<float>(4);
	m_unknownVariables.at(indexOfScreen).screenLeftZ = dstCol.at<float>(5);

	m_unknownVariables.at(indexOfScreen).screenUpX = dstCol.at<float>(6);
	m_unknownVariables.at(indexOfScreen).screenUpY = dstCol.at<float>(7);
	m_unknownVariables.at(indexOfScreen).screenUpZ = dstCol.at<float>(8);

	for (auto i = 9; i < dstCol.rows; ++i) {
		m_unknownVariables.at(indexOfScreen).distance.push_back(dstCol.at<float>(i));
	}

	return dst;
}

bool ScreenPositionRecoverer::isInScreen(const EyeFeature& eyeFeature,std::vector<POINT> & ptOut, int indexOfScreen)
{
	if (m_variables.at(indexOfScreen).size() < 15) {
		std::cout << " Screen number " << indexOfScreen << " is not ready for detection " << std::endl;
		return false;
	} 
	auto feature = eyeFeature.getFeature();
	auto modF = std::sqrt(feature.at(3) * feature.at(3) + feature.at(4) * feature.at(4) + feature.at(5) * feature.at(5));
	cv::Mat A = (cv::Mat_<float>(3, 3) << m_unknownVariables.at(indexOfScreen).screenLeftX, m_unknownVariables.at(indexOfScreen).screenUpX, feature.at(3) / modF,
		m_unknownVariables.at(indexOfScreen).screenLeftY, m_unknownVariables.at(indexOfScreen).screenUpY, feature.at(4) / modF,
		m_unknownVariables.at(indexOfScreen).screenLeftZ, m_unknownVariables.at(indexOfScreen).screenUpZ, feature.at(5)  / modF);

	cv::Mat b = (cv::Mat_<float>(3, 1) << (feature.at(0) / 1000) - m_unknownVariables.at(indexOfScreen).screenPositionX,
		(feature.at(1) / 1000) - m_unknownVariables.at(indexOfScreen).screenPositionY,
		(feature.at(2) / 1000) - m_unknownVariables.at(indexOfScreen).screenPositionZ);

	cv::Mat x = A.inv() * b;
	auto possiblePointX = feature.at(0) / 1000 + (feature.at(3) / modF) * x.at<float>(0, 2);
	auto possiblePointY = feature.at(1) / 1000 + (feature.at(4) / modF) * x.at<float>(0, 2);
	auto possiblePointZ = feature.at(2) / 1000 + (feature.at(5) / modF) * x.at<float>(0, 2);
	float minX = std::numeric_limits<float>::max(), maxX = -minX,
		minY = std::numeric_limits<float>::max(), maxY = -minY,
		minZ = std::numeric_limits<float>::max(), maxZ = -minZ;
	//std::cout << " x: [ " << minX << ", " << maxX << " ]" << std::endl;
	//std::cout << " y: [ " << minY << ", " << maxY << " ]" << std::endl;
	//std::cout << " z: [ " << minZ << ", " << maxZ << " ]" << std::endl;
	auto possiblePointIn3D = cv::Vec3f(possiblePointX, possiblePointY, possiblePointZ);
	std::vector< std::vector<cv::Vec3f>> pointsInCvVec3f;
	
	pointsInCvVec3f.push_back(getAllPoints(minX, maxX, minY, maxY, minZ, maxZ,  indexOfScreen));
	//ptOut.push_back(getNearestPoint(possiblePointIn3D, pointsInCvVec3f.at(i), indexOfScreen));
	
	//auto pt= getNeareastPointByAproximationAndGazeOrientation(possiblePointIn3D, pointsInCvVec3f,indexOfScreen);
	auto pt= getNearestPoint(possiblePointIn3D, pointsInCvVec3f.front(),indexOfScreen);
	ptOut = { pt };
	//std::cout << "Point : " << pt.x << " x " << pt.y << std::endl;
	//std::cout << " x: [ " << minX << ", " << maxX << " ]" << std::endl;
	//std::cout << " y: [ " << minY << ", " << maxY << " ]" << std::endl;
	//std::cout << " z: [ " << minZ << ", " << maxZ << " ]" << std::endl;
	//std::cout << " pos: [ " << possiblePointX << ", " << possiblePointY << ", " << possiblePointZ << " ]" << std::endl;
	if (minX <= possiblePointX && maxX >= possiblePointX
		&& minY <= possiblePointY && maxY >= possiblePointY
		//&& minZ <= possiblePointZ && maxZ >= possiblePointZ
		)
	{
		m_lastPossiblePoint = possiblePointIn3D;
		return true;
	}
	return false;
}

std::vector<POINT> ScreenPositionRecoverer::getPossiblePointByAproximation(const EyeFeature& eyeFeature, int indexOfScreen)
{
	std::vector<POINT> res; 
	for (auto i = 0; i < 3; ++i) {
		auto feature = eyeFeature.getFeature();
		int step = i * 6;
		int stepOfIndex = indexOfScreen * 3 + i;
		auto modF = std::sqrt(feature.at(step + 3) * feature.at(step + 3) + feature.at(step + 4) * feature.at(step + 4) + feature.at(step + 5) * feature.at(step + 5));
		cv::Mat A = (cv::Mat_<float>(3, 3) << m_unknownVariables.at(stepOfIndex).screenLeftX, m_unknownVariables.at(stepOfIndex).screenUpX, feature.at(step + 3) / modF,
			m_unknownVariables.at(stepOfIndex).screenLeftY, m_unknownVariables.at(stepOfIndex).screenUpY, feature.at(step + 4) / modF,
			m_unknownVariables.at(stepOfIndex).screenLeftZ, m_unknownVariables.at(stepOfIndex).screenUpZ, feature.at(step + 5) / modF);

		cv::Mat b = (cv::Mat_<float>(3, 1) << feature.at(step + 0) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionX,
			feature.at(step + 1) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionY,
			feature.at(step + 2) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionZ);

		cv::Mat x = A.inv() * b;
		std::cout << x << std::endl;
		POINT pt;
		pt.x = x.at<float>(0, 0);
		pt.y = x.at<float>(0, 1);
		res.push_back(pt);
	}
	return res;
}

void ScreenPositionRecoverer::addVariables(EyeFeatureToPointMapping eyeFeatureToPointMapping, int indexOfScreen,int positionInVector, int indexOfVariable)
{
	for (auto const& f : eyeFeatureToPointMapping) {

		KnownVariables currentKnownVariables;
		auto feature = f.first.getFeature();
		auto normalizedCoordinates = getNormalizedScreenCoordinates(f.second);

		int step = indexOfVariable * 6;

		//Head position
		
		currentKnownVariables.headPositionX = feature.at(0+step) / 1000;
		currentKnownVariables.headPositionY = feature.at(1+step) / 1000;
		currentKnownVariables.headPositionZ = feature.at(2+step) / 1000;

		//Head forward vector
		auto modF = std::sqrt(feature.at(3+step) * feature.at(3+step) + feature.at(4+ step) * feature.at(4+ step) + feature.at(5+ step) * feature.at(5+ step));
		currentKnownVariables.headForwardX = feature.at(3+step) / modF;
		currentKnownVariables.headForwardY = feature.at(4+step) / modF;
		currentKnownVariables.headForwardZ = feature.at(5+step) / modF;
		//Screen normalaized coordinates
		currentKnownVariables.coordinateX = normalizedCoordinates.first;
		currentKnownVariables.coordinateY = normalizedCoordinates.second;
		if (positionInVector * 3 + indexOfVariable < m_points.size()) {
			m_variables.at(positionInVector*3+ indexOfVariable).push_back(currentKnownVariables);// = UnknownVariables();
			m_points.at(positionInVector*3+ indexOfVariable).push_back(f.second);
		}
		
	}
}

std::vector<cv::Vec3f> ScreenPositionRecoverer::getAllPoints(float& minX, float& maxX, float& minY, float& maxY, float& minZ, float& maxZ , int indexOfScreen)
{
	std::vector<cv::Vec3f> res; 
	for (auto i = 0; i < m_variables.at(indexOfScreen).size();++i) {
		auto v = m_variables.at(indexOfScreen).at(i);
		cv::Vec3f currentVec;
		currentVec[0] = v.headPositionX + v.headForwardX * m_unknownVariables.at(indexOfScreen).distance.at(i);
		currentVec[1] = v.headPositionY + v.headForwardY * m_unknownVariables.at(indexOfScreen).distance.at(i);
		currentVec[2] = v.headPositionZ + v.headForwardZ * m_unknownVariables.at(indexOfScreen).distance.at(i);

		if (maxX < currentVec[0]) {
			maxX = currentVec[0];
		}
		if (minX > currentVec[0]) {
			minX = currentVec[0];
		}
		if (maxY < currentVec[1]) {
			maxY = currentVec[1];
		}
		if (minY > currentVec[1]) {
			minY = currentVec[1];
		}
		if (maxZ < currentVec[2]) {
			maxZ = currentVec[2];
		}
		if (minZ > currentVec[2]) {
			minZ = currentVec[2];
		}

		res.push_back(currentVec);
	}
	return res;
}

POINT ScreenPositionRecoverer::getNearestPoint(const cv::Vec3f& point, const std::vector<cv::Vec3f>& points , int indexOfScreen)
{			
	float min = std::numeric_limits<float>::max();
	auto pos = -1;
	for (auto i = 0; i < points.size();++i) {
		auto pt = points.at(i);
		auto currVal = (pt[0] - point[0]) * (pt[0] - point[0]) + (pt[1] - point[1]) * (pt[1] - point[1]) + (pt[2] - point[2]) * (pt[2] - point[2]);
		if (currVal < min) {
			min = currVal;
			pos = i;
		}
	}
	if (pos != -1 && pos < m_points.at(indexOfScreen).size())
	{
		return m_points.at(indexOfScreen).at(pos);
	}
	return POINT();
}

std::vector<POINT> ScreenPositionRecoverer::getNeareastPointByAproximationAndGazeOrientation(const cv::Vec3f& point, const std::vector<std::vector<cv::Vec3f>>& points, int indexOfScreen)
{
	std::vector<POINT> res;
	//for (auto i = 0; i < points.size();++i) {
		//std::cout << " index " << indexOfScreen << " i " <<  i  << " " <<indexOfScreen * 3 + i << std::endl;
	res.push_back(getNeareastPointByAproximation(point, points.front(), indexOfScreen));
	//}
	return res;
}

POINT ScreenPositionRecoverer::getNeareastPointByAproximation(const cv::Vec3f & point, const std::vector<cv::Vec3f>& points, int indexOfScreen)
{
	std::vector<float> distances;
	float sumOfDistances = 0.0f;
	float min = std::numeric_limits<float>::max(), max = -min;
	for (auto i = 0; i < points.size(); ++i) {
		auto pt = points.at(i);
		auto currVal = std::sqrt((pt[0] - point[0]) * (pt[0] - point[0]) + (pt[1] - point[1]) * (pt[1] - point[1]) + (pt[2] - point[2]) * (pt[2] - point[2]));
		distances.push_back(currVal);
		if (currVal < min) {
			min= currVal;
		}
		if (currVal > max) {
			max = currVal;
		}
		sumOfDistances += currVal;
	}
	float x = 0;
	float y = 0;
	float sumExp = 0;
	for (auto i = 0; i < distances.size(); ++i) {
		if (distances.at(i) < (min+max*0.1) ) {
		    distances.at(i) = max-distances.at(i);
			sumExp +=distances.at(i);
		}
		else {
			distances.at(i) = 0;
		}
		//auto val = (std::abs(distances.at(i)-max) / sumOfDistances);
		//std::cout << val << std::endl; 
	}
	for (auto i = 0; i < m_points.at(indexOfScreen).size();++i) {
		auto const& pt = m_points.at(indexOfScreen).at(i);
		auto val = distances.at(i)/sumExp;
		//std::cout << val << std::endl;
		x += (pt.x * val);
		y += (pt.y * val);
	}
	POINT neareastPoint;
	//std::cout << x << " " << y << std::endl;
	neareastPoint.x = int(x);
	neareastPoint.y = int(y);
	//std::system("pause");
	return neareastPoint;
}

float distance(const cv::Vec3f& first, const cv::Vec3f& second)
{
	auto currVal = std::sqrt((first[0] - second[0]) * (first[0] - second[0]) + (first[1] - second[1]) * (first[1] - second[1]) + (first[2] - second[2]) * (first[2] - second[2]));
	return currVal;
}

int positionOfNearestVector(const cv::Vec3f& v3f, const std::vector<cv::Vec3f>& v3fs) {
	float min = std::numeric_limits<float>::max();
	int pos = -1; 
	for (auto i = 0; i < v3fs.size();++i) {
		auto val = distance(v3fs.at(i), v3f);
		if (val< min) {
			min = val;
			pos = i;
		}
	}
	return pos;
}

std::vector<POINT> ScreenPositionRecoverer::getPointsForScreen(const EyeFeature& eyeFeature, int indexOfScreen,int & countOfMatchings)
{
	std::vector<POINT> res; 
	countOfMatchings = 0;
	for (auto i = 0; i < 3; ++i) {
		float minX = std::numeric_limits<float>::max(), maxX = -minX,
			minY = std::numeric_limits<float>::max(), maxY = -minY,
			minZ = std::numeric_limits<float>::max(), maxZ = -minZ;
		auto feature = eyeFeature.getFeature();
		int step = i * 6;
		int stepOfIndex = indexOfScreen * 3 + i;
		if (m_variables.at(stepOfIndex).size() < 15) {
			continue;
		}
		auto vec3fs = getAllPoints(minX, maxX, minY, maxY, minZ, maxZ, stepOfIndex);
		auto modF = std::sqrt(feature.at(step + 3) * feature.at(step + 3) + feature.at(step + 4) * feature.at(step + 4) + feature.at(step + 5) * feature.at(step + 5));
		cv::Mat A = (cv::Mat_<float>(3, 3) << m_unknownVariables.at(stepOfIndex).screenLeftX, m_unknownVariables.at(stepOfIndex).screenUpX, feature.at(step + 3) / modF,
			m_unknownVariables.at(stepOfIndex).screenLeftY, m_unknownVariables.at(stepOfIndex).screenUpY, feature.at(step + 4) / modF,
			m_unknownVariables.at(stepOfIndex).screenLeftZ, m_unknownVariables.at(stepOfIndex).screenUpZ, feature.at(step + 5) / modF);


		cv::Mat b = (cv::Mat_<float>(3, 1) << feature.at(step + 0) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionX,
			feature.at(step + 1) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionY,
			feature.at(step + 2) / 1000 - m_unknownVariables.at(stepOfIndex).screenPositionZ);

		cv::Mat x = A.inv() * b;
		auto possiblePointX = feature.at(step+0) / 1000 + (feature.at(step+3) / modF) * x.at<float>(0, 2);
		auto possiblePointY = feature.at(step+1) / 1000 + (feature.at(step+4) / modF) * x.at<float>(0, 2);
		auto possiblePointZ = feature.at(step+2) / 1000 + (feature.at(step+5) / modF) * x.at<float>(0, 2);
		res.push_back(m_points.at(stepOfIndex).at(positionOfNearestVector(cv::Vec3f(possiblePointX, possiblePointY, possiblePointZ), vec3fs)));
		if (minX <= possiblePointX && maxX >= possiblePointX
			&& minY <= possiblePointY && maxY >= possiblePointY
			//&& minZ <= possiblePointZ && maxZ >= possiblePointZ
			)
		{
			countOfMatchings++;
		}

	}

	return res;
}

std::pair<float, float> ScreenPositionRecoverer::getPossiblePoint(const EyeFeature& eyeFeature, int indexOfScreen)
{
	auto feature = eyeFeature.getFeature();
	int step = indexOfScreen * 3;
	auto modF = std::sqrt(feature.at(step + 3) * feature.at(step + 3) + feature.at(step + 4) * feature.at(step + 4) + feature.at(step + 5) * feature.at(step + 5));
	cv::Mat A = (cv::Mat_<float>(3, 3) << m_unknownVariables.at(indexOfScreen).screenLeftX, m_unknownVariables.at(indexOfScreen).screenUpX, feature.at(step + 3)/ modF,
										  m_unknownVariables.at(indexOfScreen).screenLeftY, m_unknownVariables.at(indexOfScreen).screenUpY, feature.at(step + 4)/ modF,
										  m_unknownVariables.at(indexOfScreen).screenLeftZ, m_unknownVariables.at(indexOfScreen).screenUpZ, feature.at(step + 5)/ modF);

	cv::Mat b = (cv::Mat_<float>(3, 1) << feature.at(step + 0)/1000 - m_unknownVariables.at(indexOfScreen).screenPositionX,
										  feature.at(step + 1)/1000 - m_unknownVariables.at(indexOfScreen).screenPositionY,
										  feature.at(step + 2)/1000 - m_unknownVariables.at(indexOfScreen).screenPositionZ);
	
	cv::Mat x = A.inv() * b;
	std::cout << x << std::endl;
	return std::pair<float, float>(x.at<float>(0,0), x.at<float>(0,1));
}

void ScreenPositionRecoverer::printMat(int indexOfScreen)
{	
	int index = 0; 
	for (auto i = 0; i < m_variables.at(indexOfScreen).size(); ++i) {
		
		auto varsX = m_variables.at(indexOfScreen).at(i).getRow(Axis::eX, i, m_variables.size());
		auto varsY = m_variables.at(indexOfScreen).at(i).getRow(Axis::eY, i, m_variables.size());
		auto varsZ = m_variables.at(indexOfScreen).at(i).getRow(Axis::eZ, i, m_variables.size());
		std::copy(varsX.begin(), varsX.end(), std::ostream_iterator<float>(std::cout, ", "));
		std::cout << std::endl;
		std::copy(varsY.begin(), varsY.end(), std::ostream_iterator<float>(std::cout, ", "));
		std::cout << std::endl;
		std::copy(varsZ.begin(), varsZ.end(), std::ostream_iterator<float>(std::cout, ", "));
		std::cout << std::endl;
	}
}
std::vector<cv::Mat> ScreenPositionRecoverer::getPossibleMatrixes()
{
	//std::vector<cv::Mat> res;
	//std::vector<KnownVariables> knownVariables; 
	//for (auto i = 0; i < m_variables.size(); ++i) {
	//	knownVariables.clear();
	//	knownVariables.push_back(m_variables.at(i));
	//	for (auto j = i; j < m_variables.size(); ++j) {
	//		knownVariables.push_back(m_variables.at(i));
	//		if (knownVariables.size() == 5) {
	//			break;
	//		}
	//	}
	//	if (knownVariables.size() == 5) {
	//		res.push_back(createMat(knownVariables));
	//	}
	//}
	//return res;
	return std::vector<cv::Mat>();
}
cv::Mat ScreenPositionRecoverer::createMat(cv::Mat& _mat, int indexOfScreen)
{
	_mat = cv::Mat(m_variables.at(indexOfScreen).size() * 3, 9 + m_variables.at(indexOfScreen).size(), CV_32FC1).clone();
	int index = 0;
	for (auto i = 0; i < m_variables.at(indexOfScreen).size(); ++i) {

		auto vars = m_variables.at(indexOfScreen).at(i).getRow(Axis::eX, i, m_variables.at(indexOfScreen).size());
		for (auto j = 0; j < vars.size(); ++j) {
			_mat.at<float>(index, j) = vars.at(j);
		}
		index++;
		vars = m_variables.at(indexOfScreen).at(i).getRow(Axis::eY, i, m_variables.at(indexOfScreen).size());
		for (auto j = 0; j < vars.size(); ++j) {
			_mat.at<float>(index, j) = vars.at(j);
		}
		index++;
		//if (index >= 13) {
		//	break;
		//}
		vars = m_variables.at(indexOfScreen).at(i).getRow(Axis::eZ, i, m_variables.at(indexOfScreen).size());
		for (auto j = 0; j <vars.size(); ++j) {
			_mat.at<float>(index, j) = vars.at(j);
		}
		index++;
	}
	return _mat;
}
cv::Mat ScreenPositionRecoverer::matEqualTo(int indexOfScreen)
{
	cv::Mat mat(3 * m_variables.at(indexOfScreen).size(),1, CV_32FC1);
	int index = 0;
	for (auto v : m_variables.at(indexOfScreen)) {
		auto pos = v.getHeadPosition();
		for (auto i = 0; i < pos.size(); ++i) {
			mat.at<float>(index++, 0) = pos.at(i);
		}
		//if (index >= 13) {
		//	break;
		//}
	}
	return mat;
}
void ScreenPositionRecoverer::writeResults(const std::string& str, int indexOfScreen)
{
	std::ofstream of(str.c_str());
	for (auto i = 0; i < m_variables.at(indexOfScreen).size();++i) {
		auto v = m_variables.at(indexOfScreen).at(i);
		of << v.headPositionX << " " << v.headPositionY << " " << v.headPositionZ << " " <<v.headForwardX << " " <<v.headForwardY << " " <<v.headForwardZ << " " << m_unknownVariables.at(indexOfScreen).distance[i] <<  "\n";
	}
}
cv::Mat ScreenPositionRecoverer::createMat(const std::vector<KnownVariables>& variables)
{
	cv::Mat mat(9 + variables.size(), 9 + variables.size(), CV_32FC1);
	int index = 0;
	for (auto i = 0; i < variables.size(); ++i) {

		auto vars = variables.at(i).getRow(Axis::eX, i, variables.size());
		for (auto j = 0; j < mat.cols; ++j) {
			mat.at<float>(index, j) = vars.at(j);
		}
		index++;
		vars = variables.at(i).getRow(Axis::eY, i, variables.size());
		for (auto j = 0; j < mat.cols; ++j) {
			mat.at<float>(index, j) = vars.at(j);
		}
		index++;
		vars = variables.at(i).getRow(Axis::eZ, i, variables.size());
		if (index >= 13) {
			break;
		}
		for (auto j = 0; j <mat.cols; ++j) {
			mat.at<float>(index, j) = vars.at(j);
		}
		index++;
	}
	return mat;
}
cv::Mat ScreenPositionRecoverer::matEqualTo(const std::vector<KnownVariables>& variables)
{
	cv::Mat mat(3 * variables.size() - 1, 1, CV_32FC1);
	int index = 0;
	for (auto v : variables) {
		auto pos = v.getHeadPosition();
		for (auto i = 0; i < pos.size(); ++i) {
			mat.at<float>(index++, 0) = pos.at(i);
		}
		if (index >= 13) {
			break;
		}
	}
	return mat;
}
#include <iostream> 
std::pair<float, float> ScreenPositionRecoverer::getNormalizedScreenCoordinates(const POINT & pt)
{
	std::pair<float, float > res; 
	//std::cout <<"POINT:\t" <<  pt.x << " " << pt.y << std::endl;
	res.first =  (2 * float(pt.x) - s_width ) / s_width;
	res.second = (2 * float(pt.y) - s_height) / s_width;
	//std::cout <<"SCREE:\t" << res.first << " " << res.second << std::endl;

	return res;
}

std::vector<float> KnownVariables::getRow(const Axis & axis,int variablePosition ,int countOfPoints) const 
{
	std::vector<float> res(9+countOfPoints,0);
	res[axis+0]=1;
	if (axis == eX) {
		res[3] = coordinateX;
	}
	if (axis == eY) {
		res[4] = coordinateY;
	}

	switch (axis)
	{
	case eX:
		res[5 +  variablePosition] = -headForwardX;
		break;
	case eY:
		res[5 +  variablePosition] = -headForwardY;
		break;
	case eZ:
		res[5 +  variablePosition] = -headForwardZ;
		break;

	default:
		break;
	}
	return res;
}

std::vector<float> KnownVariables::getHeadPosition() const
{
	//std::cout << headPositionX << " " << headPositionY << " " << headPositionZ << std::endl;
	return std::vector<float>{headPositionX,headPositionY,headPositionZ};
}
