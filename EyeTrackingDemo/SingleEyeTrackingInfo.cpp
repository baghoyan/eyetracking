#include "SingleEyeTrackingInfo.h"

size_t splitString(const std::string &txt, std::vector<std::string> &strs, char ch)
{
	size_t pos = txt.find(ch);
	size_t initialPos = 0;
	strs.clear();

	while (pos != std::string::npos) {
		strs.push_back(txt.substr(initialPos, pos - initialPos));
		initialPos = pos + 1;

		pos = txt.find(ch, initialPos);
	}

	strs.push_back(txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1));

	return strs.size();
}

EyeFeature SingleEyeTrackingInfo::getEyeFeature() const
{
	EyeFeature eyeFeature(m_featureVector);
	eyeFeature.setId(m_id);

	return eyeFeature;
}

POINT SingleEyeTrackingInfo::getPoint() const 
{
	if (m_mousePosition.empty()) {
		return POINT();
	}
	std::vector<std::string> strs;
	splitString(m_mousePosition, strs, ' ');
	POINT res;
	res.x = std::stoi(strs.at(0));
	res.y = std::stoi(strs.at(1));
	return res;
}
