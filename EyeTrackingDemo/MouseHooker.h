#pragma once
#include <Windows.h>
#include "MonitorWidget.h"
#include <qobject.h>

#include <mutex>
#include <iostream>

class MouseListener;
using MouseListenerPtr = MouseListener*;
struct WindowSize
{
    WindowSize(long left, long right,long bottom,long top,long numberOfScreen) : left_(left) , right_(right) , bottom_(bottom) , top_(top), numberOfScreen_(numberOfScreen) 
    {
        w_ = right_ - left_;
        h_ = top_ - bottom_;
    }
    bool getPointOnScreen(const POINT& p, int& xOut, int& yOut);
    bool getPointOnScreen(int xIn, int yIn, int& xOut, int& yOut);
    bool isPointInScreen(const POINT & p);
    bool isPointInScreen(int x, int y);
    long left_;
    long right_;
    long bottom_;
    long top_;

    long h_;
    long w_;
     
    int numberOfScreen_;
};

//#ifdef _PRINTL_
//#define printl(x,y) std::cout << x << " " << y << std::endl;
//#define printl(x) std::cout << x << std::endl;
//#else
//#define printl(x,y) 
//#define printl(x) 
//#endif // !_PRINTL_


class MouseListener : public QObject {
    Q_OBJECT
signals:
    void clicked(int x, int y);
    void clickOnScreen(int screenNumber, int x, int y);
public:
    MouseListener() {  };
    static MouseListenerPtr getInstance();
    int MonitorCount();
    void appendScreen(long left, long right, long bottom, long top, long numberOfScreen);
    void clickedOn(int x, int y)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        //std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
        MonitorCount();
        //std::cout << "SD " << m_screenSizes.size() << std::endl;
        int sX, sY;
        //std::cout << "Clicked : " << x << " " << y << std::endl;
        for (auto i = 0; i < m_screenSizes.size(); ++i) {
            if (m_screenSizes.at(i).getPointOnScreen(x, y, sX, sY)) {
                //std::cout << i << "-th screen pos: [ " << sX << " : " << sY << " ]" <<std::endl;
                emit(clickOnScreen(i, sX, sY));
                break;
            }
        }
        emit(clicked(x, y));
    }
private:
    std::vector<WindowSize> m_screenSizes;
    std::mutex m_mutex;
    

};

void initialize();
