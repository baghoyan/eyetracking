#include "EyeTrackingReporter.h"
#include <qdebug.h>
#include "ScreenPositionRecoverer.h"

EyeTrackingReporter::EyeTrackingReporter(int sec, QThread* parent)
	:m_thread(parent)
	, m_sec(sec)
{
	this->moveToThread(m_thread);
	m_timer = new QTimer;
	m_timer->moveToThread(m_thread);

	QObject::connect(m_timer, &QTimer::timeout, this, &EyeTrackingReporter::onTimeout);
	start();
}


void EyeTrackingReporter::start()
{
	QMetaObject::invokeMethod(m_timer, "start", Q_ARG(int, m_sec));
}

void EyeTrackingReporter::stop()
{
	QMetaObject::invokeMethod(m_timer, "stop");
}

void EyeTrackingReporter::onTimeout()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	emit(detectScreen());
}

void EyeTrackingReporter::setFeature(const EyeFeature& eyeFeature)
{
	m_feature = eyeFeature;
}

void EyeTrackingReporter::setConfidence(double confidence)
{
	m_confidence = confidence;
}
