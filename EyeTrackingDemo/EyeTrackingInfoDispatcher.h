#pragma once
#include <Windows.h>

#include "DBManager.h"
#include "SingleEyeTrackingInfo.h"
#include "EyeFeature.h"
#include <QObject>
#include <QString>

#include <mutex>
typedef std::map<EyeFeature,POINT> EyeFeatureToPointMapping;
typedef std::vector<EyeFeatureToPointMapping> EyeFeatureToPointMappingVector;
class EyeTrackingInfoDispatcher;
typedef EyeTrackingInfoDispatcher* EyeTrackingInfoDispatcherPtr;
struct pointComperator {
	bool operator() (const cv::Point2f& pt1, const cv::Point2f& pt2) const {
		if (pt1.x < pt2.x) {
			return true;
		}
		else if (pt1.x == pt2.x) {
			return true;
		}
		return false;
	}
};
class EyeTrackingInfoDispatcher : public QObject
{
	Q_OBJECT
public:

	static EyeTrackingInfoDispatcherPtr getInstance();

	void setAddingState(bool b);
	void appendNewInfo(const SingleEyeTrackingInfo & singleEyeTrackingInfo);
	void testNewFeature(const EyeFeature & eyeFeature);
	void testFeature(const EyeFeature & eyeFeature);
	cv::Mat getMatOfPositiveEyeFeatures(int & positiveFeatures , int & negativeFeatures);
	void showAllAvailablePositivePoints();

#ifndef SINGLE_SCREEN
	EyeFeatureToPointMappingVector getEyeFeaturesAndPoints() const;
#else
	EyeFeatureToPointMapping getEyeFeaturesAndPoints() const;
#endif
signals:
	void availablePoints(int x, int y);
	void onScreen(int x, int y);
	void notOnScreen(QString message);
	void availableDataChanged(int screenNumber, int value);
private:
	EyeTrackingInfoDispatcher();
	void computeNormalizeVector(const std::vector<double> & feature);
	POINT getNearestFeaturePoint(const EyeFeature & eyeFeature);
	bool isWatchingOnScreen(const std::vector<double> & newFeature, const std::vector<double>& screenCenter, const std::vector<double>& outScreenCenter);
	double getEuclidDistance(const std::vector<double> & first, const std::vector<double>& second);

	std::vector<double> getCenter(cv::Mat featureMat);
	cv::Mat getMatOfEyeFeatures(EyeFeatureToPointMapping eyeFeatures);
	void appendNewInfoHelper(const SingleEyeTrackingInfo & singleEyeTrackingInfo);

	void addFeatureToCVPoints(const EyeFeature & eyeFeature);
private:
	static EyeTrackingInfoDispatcherPtr s_instance;
	std::mutex m_eyeFeatureToPointsMutex;
	bool m_addData;
	int m_sizeOfData;
	std::vector<double> m_maxFeatureForPositives;
	EyeFeatureToPointMappingVector m_featuresWithScreens; 
	EyeFeatureToPointMapping m_eyeFeatureToPointMappingPositiveCases;
	EyeFeatureToPointMapping m_eyeFeatureToPointMappingNegativeCases;
	DBManager* m_dbManager;
	int m_lastId;
	std::vector<int> m_countsOfScreen;
	std::vector<std::vector<cv::Point2f>>m_points;
};