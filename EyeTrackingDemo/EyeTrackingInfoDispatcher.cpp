#include "EyeTrackingInfoDispatcher.h"
#include "DBManager.h"

EyeTrackingInfoDispatcherPtr EyeTrackingInfoDispatcher::s_instance = nullptr;

void EyeTrackingInfoDispatcher::computeNormalizeVector(const std::vector<double>& feature)
{
	size_t size = feature.size() > m_maxFeatureForPositives.size() ? m_maxFeatureForPositives.size() : feature.size();
	for (auto i = 0; i < size; ++i) {
		if (feature.at(i)>m_maxFeatureForPositives.at(i)) {
			m_maxFeatureForPositives.at(i) = feature.at(i);
		}
	}
}

POINT EyeTrackingInfoDispatcher::getNearestFeaturePoint(const EyeFeature & eyeFeature)
{
	POINT res; 
	double min = std::numeric_limits<int>::max();
#ifndef SINGLE_SCREEN
	for (auto const& eyeFeatureToPointMapping : m_featuresWithScreens) {
#else
	auto eyeFeatureToPointMapping = m_eyeFeatureToPointMappingPositiveCases;
#endif // SINGLE_SCREEN

	for (auto const& e : eyeFeatureToPointMapping) {
		//auto currVal = eyeFeature.distance(e.first);
		auto eyeFeature1Copy = eyeFeature;
		auto eyeFeature2Copy = e.first;
		eyeFeature1Copy.normalaizeFeature(m_maxFeatureForPositives);
		eyeFeature2Copy.normalaizeFeature(m_maxFeatureForPositives);
		//std::cout << "\neye Feature " << std::endl;
		//std::cout << eyeFeature1Copy;
		//std::cout << "\nnth Feature " << std::endl;
		//std::cout << eyeFeature2Copy;

		auto currVal = eyeFeature1Copy.distance(eyeFeature2Copy);
		if (currVal < min) {
			res = e.second;
			min = currVal;
		}
	}
#ifndef SINGLE_SCREEN
	}
#endif // SINGLE_SCREEN
	return res;
}

bool EyeTrackingInfoDispatcher::isWatchingOnScreen(const std::vector<double>& newFeature, const std::vector<double>& screenCenter, const std::vector<double>& outScreenCenter)
{
	auto screenDistance = getEuclidDistance(newFeature, screenCenter);
	auto outScreenDistance = getEuclidDistance(newFeature, outScreenCenter);
	return screenDistance < outScreenDistance;
}

double EyeTrackingInfoDispatcher::getEuclidDistance(const std::vector<double>& first, const std::vector<double>& second)
{
	double res;
	size_t size = first.size() > second.size() ? second.size() : first.size();
	for (auto i = 0; i < size; ++i) {
		res += (first.at(i) - second.at(i))*(first.at(i) - second.at(i));
	}
	return res;
}

EyeTrackingInfoDispatcher::EyeTrackingInfoDispatcher()
	:m_maxFeatureForPositives(12, -999999.99)
	, m_points(1000)
	, m_countsOfScreen(100)
{
	m_sizeOfData = 100;
	m_featuresWithScreens.resize(2);
	m_addData = false;
	m_dbManager = new DBManager;
	std::lock_guard<std::mutex> lock(m_eyeFeatureToPointsMutex);
	auto availableData = m_dbManager->getAvailableEyeTrackingData();
	std::cout << " Loading available data..." << availableData.size()<< std::endl;
	for (auto const & d : availableData) {
		appendNewInfoHelper(d);
	}
	std::cout << " Data successfully loaded" << std::endl;
	for (auto const& f : m_featuresWithScreens) {
		for (auto const& g : f) {
			std::cout << g.first.getId() << std::endl;
		}
		std::cout << " __________________________________________  "<<f.size() << std::endl;
	}
#ifndef SINGLE_SCREEN
	for (auto i = 0; i < m_featuresWithScreens.size();++i) {
		std::cout << "Data for " << i << " screen " << m_featuresWithScreens.at(i).size() << std::endl;
	}
#else
	std::cout << " Data Size Positive" << m_eyeFeatureToPointMappingPositiveCases.size()<< std::endl;
#endif // SINGLE_SCREEN
	std::cout << " Data Size Negative" << m_eyeFeatureToPointMappingNegativeCases.size()<< std::endl;
}

std::vector<double> EyeTrackingInfoDispatcher::getCenter(cv::Mat featureMat)
{
	cv::Mat labels;
	cv::Mat centers;
	cv::kmeans(featureMat, 1, labels, cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 1000, 0.001), 10, cv::KMEANS_PP_CENTERS, centers);
	std::vector<double> res;
	for (int i = 0; i < centers.rows; ++i)
	{
		for (auto j = 0; j < centers.cols; ++j) {
			res.push_back(centers.at<float>(i, j));
		}
	}
	return res;
}

cv::Mat EyeTrackingInfoDispatcher::getMatOfEyeFeatures(EyeFeatureToPointMapping eyeFeatures)
{
	auto begin = eyeFeatures.begin();
	cv::Mat eyeFeaturesMat = cv::Mat(eyeFeatures.size()+m_eyeFeatureToPointMappingNegativeCases.size(), begin->first.getFeature().size(), CV_32FC1, cv::Scalar::all(255));
	auto i = 0;
	for (; i < eyeFeatures.size(); ++i, begin++) {
		for (auto j = 0; j < eyeFeaturesMat.cols; ++j) {
			eyeFeaturesMat.at<float>(i, j) = begin->first.getFeature().at(j);
		}
	}
	begin = m_eyeFeatureToPointMappingNegativeCases.begin();
	for (; i < eyeFeaturesMat.rows; ++i, begin++) {
		for (auto j = 0; j < eyeFeaturesMat.cols; ++j) {
			eyeFeaturesMat.at<float>(i, j) = begin->first.getFeature().at(j);
		}
	}
	return eyeFeaturesMat;
}

EyeTrackingInfoDispatcherPtr EyeTrackingInfoDispatcher::getInstance()
{
	if (s_instance == nullptr) {
		s_instance = new EyeTrackingInfoDispatcher;
	}
	return s_instance;
}

void EyeTrackingInfoDispatcher::setAddingState(bool b)
{
	m_addData = b;
}

void EyeTrackingInfoDispatcher::appendNewInfo(const SingleEyeTrackingInfo & singleEyeTrackingInfo)
{
	if (m_addData) {
		for (auto f : m_featuresWithScreens) {
			std::cout << f.size() << " " << f.size() << std::endl;
		}
		{
			std::lock_guard<std::mutex> lock(m_eyeFeatureToPointsMutex);
			appendNewInfoHelper(singleEyeTrackingInfo);
		}
		m_dbManager->addEyeTrackingInfo(singleEyeTrackingInfo);
	}
}

void EyeTrackingInfoDispatcher::testNewFeature(const EyeFeature & eyeFeature)
{
	if (m_eyeFeatureToPointMappingNegativeCases.size() < 4 || m_eyeFeatureToPointMappingPositiveCases.size() <3) {
		return;
	}
	auto positiveFeaturesMat = getMatOfEyeFeatures(m_eyeFeatureToPointMappingPositiveCases);
	auto positiveFeatures = getCenter(positiveFeaturesMat);
	auto negativeFeaturesMat = getMatOfEyeFeatures(m_eyeFeatureToPointMappingNegativeCases);
	auto negativeFeatures = getCenter(negativeFeaturesMat);
	/*std::cout << "_____________" << std::endl;
	for (auto const & p : positiveFeatures) {
		std::cout << p << " ";
	}
	std::cout << "\n_____________" << std::endl;
	for (auto const & p : negativeFeatures) {
		std::cout << p << " ";
	}*/
	std::cout << std::endl;
	auto newFeature = eyeFeature.getFeature();
	if (isWatchingOnScreen(newFeature, positiveFeatures, negativeFeatures)) {
		POINT pt =getNearestFeaturePoint(eyeFeature);
		std::cout << "is on screen " << std::endl;
		emit(onScreen(pt.x, pt.y));
	}
	else {
		std::cout << "is not on screen " << std::endl;
		emit(notOnScreen("Distrubted"));
	}

}

void print(const std::vector<cv::Point2f> & pts) {
	for (auto const & pt : pts) {
		std::cout << pt;
	}
}

void EyeTrackingInfoDispatcher::testFeature(const EyeFeature & eyeFeature)
{
	bool checker = false;
	std::cout << __FUNCTION__ << " " << __LINE__ << std::endl;
	auto feature = eyeFeature.getFeature();
	std::cout << __FUNCTION__ << " " << __LINE__ << std::endl;
	int counter = 0;
	for (auto i = 0; i < feature.size(); ++i) {
		if (!checker) {
			break;
		}
		for (auto j = i + 1; j < feature.size(); ++j) {
			auto pt = cv::Point2f(feature.at(i), feature.at(j));
			std::cout << " POINT " << pt << std::endl;
			auto key = cv::Point(i, j);
			std::cout << "Size Of Points " << m_points.at(counter).size() << std::endl;
			if (cv::pointPolygonTest(m_points.at(counter), pt, true) >= 0) {
				checker = false;
				break;
			}
			counter++;
		}
	}
	std::cout << __FUNCTION__ << " " << __LINE__ << std::endl;
	if (checker) {
		POINT pt = getNearestFeaturePoint(eyeFeature);
		std::cout << "is on screen " << std::endl;
		emit(onScreen(pt.x, pt.y));
	}
	else {
		std::cout << "is not on screen " << std::endl;
		emit(notOnScreen("Distrubted"));
	}
}

cv::Mat EyeTrackingInfoDispatcher::getMatOfPositiveEyeFeatures(int & positiveFeatures, int & negativeFeatures)
{
	positiveFeatures = m_eyeFeatureToPointMappingPositiveCases.size();
	negativeFeatures = m_eyeFeatureToPointMappingNegativeCases.size();
	return getMatOfEyeFeatures(m_eyeFeatureToPointMappingPositiveCases);
}

void EyeTrackingInfoDispatcher::showAllAvailablePositivePoints()
{
	for (auto i = 0; i < m_countsOfScreen.size(); ++i) {
		if (m_countsOfScreen.at(i) != 0) {
			emit(availableDataChanged(i, ++m_countsOfScreen.at(i)));
		}
	}
	//for (auto const & e : m_eyeFeatureToPointMappingPositiveCases) {
	//	emit(availablePoints(e.second.x, e.second.y));
	//}
}
#ifndef SINGLE_SCREEN
EyeFeatureToPointMappingVector EyeTrackingInfoDispatcher::getEyeFeaturesAndPoints() const
{
	return m_featuresWithScreens;
}
#else
EyeFeatureToPointMapping EyeTrackingInfoDispatcher::getEyeFeaturesAndPoints() const
{
	return m_eyeFeatureToPointMappingPositiveCases;
}
#endif

void EyeTrackingInfoDispatcher::appendNewInfoHelper(const SingleEyeTrackingInfo & singleEyeTrackingInfo)
{
	//std::cout << singleEyeTrackingInfo.m_featureVector;
	emit(availableDataChanged(singleEyeTrackingInfo.m_screenNumber, ++m_countsOfScreen.at(singleEyeTrackingInfo.m_screenNumber)));
	std::cout << "singleEyeTrackingInfo.m_screenNumber " << singleEyeTrackingInfo.m_screenNumber << std::endl;
	if (singleEyeTrackingInfo.m_isPositiveData) {
		auto pt = singleEyeTrackingInfo.getPoint();

		addFeatureToCVPoints(singleEyeTrackingInfo.getEyeFeature());
		auto eyeFeature = singleEyeTrackingInfo.getEyeFeature();
		if (eyeFeature.getId() == -1) {
			eyeFeature.setId(m_lastId + 1);
		}
#ifndef SINGLE_SCREEN
		m_featuresWithScreens.at(singleEyeTrackingInfo.m_screenNumber)[eyeFeature] = pt;
		m_lastId = eyeFeature.getId();
		if (m_featuresWithScreens.at(singleEyeTrackingInfo.m_screenNumber).size() > m_sizeOfData) {
			auto it = m_featuresWithScreens.at(singleEyeTrackingInfo.m_screenNumber).begin();
			m_featuresWithScreens.at(singleEyeTrackingInfo.m_screenNumber).erase(it);
		}
#else
		m_eyeFeatureToPointMappingPositiveCases[singleEyeTrackingInfo.getEyeFeature()] =pt;
#endif // SINGLE_SCREEN
	}
	//else {
	//	auto currentEyeFeature = singleEyeTrackingInfo.getEyeFeature();
	//	computeNormalizeVector(currentEyeFeature.getFeature());
	//	if (m_eyeFeatureToPointMappingPositiveCases.find(currentEyeFeature) == m_eyeFeatureToPointMappingPositiveCases.end()) {
	//		m_eyeFeatureToPointMappingNegativeCases[currentEyeFeature] = singleEyeTrackingInfo.getPoint();
	//	}
	//}
}

void EyeTrackingInfoDispatcher::addFeatureToCVPoints(const EyeFeature & eyeFeature)
{
	int counter = 0;
	auto feature = eyeFeature.getFeature();
	for (auto i = 0; i < feature.size();++i) {
		for (auto j = i+1; j < feature.size(); ++j) {
			auto pt = cv::Point2f(feature.at(i), feature.at(j));
			auto key = cv::Point(i, j);
			m_points.at(counter).push_back(pt);
			counter++;
		}
	}
}
