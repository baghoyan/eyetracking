#pragma once
#include <QGraphicsView>
#include <QWidget>
#include <QScrollBar>
#include <QGraphicsSimpleTextItem>
#include <QWidget>
#include <qdebug.h>
#include <QWheelEvent>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>

class SingleImageInfoViewer;

class MonitorWidget : public QWidget
{
	Q_OBJECT
public:
	explicit MonitorWidget(int countOfMonitors, QPixmap monitorPixmap , QWidget* parent = nullptr);
	void setGoodCasePixmap(const QPixmap& pix);
	void setBadCasePixmap(const QPixmap& pix);
private:
	void createMembers();
	void setupLayouts();
	void makeConnections();
public slots:
	void onDrawPointOnScreen(int screenNumber, int x, int y);
	void onDrawPointOnScreenMultiPoints(int screenNumber, QVector<QPointF> points);
private:
	int m_countOfMonitors;
	int m_colorCollector;
	int m_lastScreen;

	QPixmap m_pixmap;
	QPixmap m_badCasePixmap;
	QPixmap m_goodCasePixmap;
	QVector<SingleImageInfoViewer*> m_singleMonitor;

};

class SingleImageInfoViewer : public QGraphicsView
{
	Q_OBJECT
public:
	SingleImageInfoViewer(QWidget* parent = nullptr) : QGraphicsView(parent)
	{
		m_backgroundImage = nullptr;

		setScene(new QGraphicsScene);
		setBackgroundBrush(QBrush(Qt::white, Qt::SolidPattern));
		setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

		setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	}

	QString generateNewUniqueId(int len = 12)
	{
		QString r;
		for (int i = 0; i < len; ++i) {
			r += QChar(qrand() % 20 + 'A');
		}
		return r;
	}

	


	void resizeEvent(QResizeEvent* ev)
	{
		if (m_backgroundImage != nullptr) {
			fitInView(m_backgroundImage, Qt::KeepAspectRatio);
			ensureVisible(m_backgroundImage, 0, 0);
			centerOn(m_backgroundImage->sceneBoundingRect().center());
		}
		QGraphicsView::resizeEvent(ev);

	}
	QString addPointAndClearOtherPoints(QPointF point, QColor c)
	{
		auto iter = m_overlays.begin();
		while (iter != m_overlays.end()) {
			
			scene()->removeItem(iter.key());
			m_overlays.remove(iter.key());
			delete iter.key();
			iter++;
		}
		QGraphicsEllipseItem* gLI = new QGraphicsEllipseItem(point.x() - 5, point.y() - 5, 10, 10, m_backgroundImage);
		gLI->setPen(c);

		gLI->setOpacity(0.85);
		m_overlays[gLI] = "";
		QString newUniqueId = generateNewUniqueId();
		gLI->setData(0, newUniqueId);
		return newUniqueId;
	}
	QString addPointAA(QPointF point, QColor c)
	{
		QGraphicsEllipseItem* gLI = new QGraphicsEllipseItem(point.x() - 5, point.y() - 5, 10, 10, m_backgroundImage);
		gLI->setPen(c);

		gLI->setOpacity(0.85);
		//m_overlays[gLI] = "";
		QString newUniqueId = generateNewUniqueId();
		gLI->setData(0, newUniqueId);
		//m_uniqueIdToOverlayMapper[newUniqueId] = gLI;
		return newUniqueId;
	}
	QString addNamedPolygon(QString name, QPolygon p, QColor c)
	{
		if (m_backgroundImage == nullptr) {
			return "";
		}
		QGraphicsPolygonItem* gPI = new QGraphicsPolygonItem(p, m_backgroundImage);
		gPI->setPen(QPen(QBrush(c), 4));
		gPI->setBrush(QBrush(c, Qt::SolidPattern));
		gPI->setOpacity(0.5);
		gPI->setToolTip(name);
		//qDebug() << "NAME " << name;
		auto splited = name.split(" ");
		QString onlyName;
		if (splited.size() <= 1) {
			onlyName = name;
		}
		else {
			splited.pop_back();
			onlyName = splited.join(" ");
		}
		QGraphicsSimpleTextItem* it = new QGraphicsSimpleTextItem(onlyName, gPI);
		it->setBrush(QBrush(Qt::white));
		it->setPen(QPen(QBrush(Qt::black), 2));
		it->setFont(QFont("arial", 30));
		it->setOpacity(1.0);
		it->setPos(gPI->boundingRect().center());
		m_overlays[gPI] = name;
		QString newUniqueId = generateNewUniqueId();
		gPI->setData(0, newUniqueId);
		m_uniqueIdToOverlayMapper[newUniqueId] = gPI;
		return newUniqueId;
	}
	QString addTextAndClearOtherPoints(QString str, QColor c)
	{
		auto iter = m_overlays.begin();
		while (iter != m_overlays.end()) {

			scene()->removeItem(iter.key());
			m_overlays.remove(iter.key());
			delete iter.key();
			iter++;
		}
		QGraphicsTextItem* gLI = new QGraphicsTextItem(str);
		//gLI->setPen(c);

		gLI->setOpacity(0.85);
		m_overlays[gLI] = "";
		QString newUniqueId = generateNewUniqueId();
		gLI->setData(0, newUniqueId);
		return newUniqueId;
	}
	QString addPoint(QPointF point, QColor c, QString name = "UNKNOWNNAME")
	{
		QGraphicsEllipseItem* gLI = new QGraphicsEllipseItem(point.x() - 5, point.y() - 5, 10, 10, m_backgroundImage);
		gLI->setPen(c);

		gLI->setOpacity(0.85);
		m_overlays[gLI] = "";
		QString newUniqueId = generateNewUniqueId();
		gLI->setData(0, newUniqueId);
		m_uniqueIdToOverlayMapper[newUniqueId] = gLI;
		QGraphicsSimpleTextItem* it = new QGraphicsSimpleTextItem(name, gLI);
		it->setBrush(QBrush(Qt::white));
		it->setPen(QPen(QBrush(Qt::black), 2));
		it->setFont(QFont("arial", 12));
		it->setOpacity(1.0);
		it->setPos(gLI->boundingRect().center());
		m_overlays[gLI] = name;
//		QString newUniqueId = generateNewUniqueId();
	//	gLI->setData(0, newUniqueId);
//		m_uniqueIdToOverlayMapper[newUniqueId] = gLI;
		return newUniqueId;
	}

	
	
	void mouseDoubleClickEvent(QMouseEvent* event)
	{
		if (m_backgroundImage != nullptr) {
			fitInView(m_backgroundImage, Qt::KeepAspectRatio);
			ensureVisible(m_backgroundImage, 0, 0);
			centerOn(m_backgroundImage->sceneBoundingRect().center());
		}
		QGraphicsView::mouseDoubleClickEvent(event);
	}

	void wheelEvent(QWheelEvent* event)
	{
		const QPointF p0scene = mapToScene(event->pos());

		qreal factor = std::pow(1.001, event->delta());
		scale(factor, factor);

		const QPointF p1mouse = mapFromScene(p0scene);
		const QPointF move = p1mouse - event->pos(); // The move
		horizontalScrollBar()->setValue(move.x() + horizontalScrollBar()->value());
		verticalScrollBar()->setValue(move.y() + verticalScrollBar()->value());
		//horizontalScrollBar()->setVisible(false);
		//verticalScrollBar()->setVisible(false);
	}
	QSize sizeHint()
	{
		return QSize(480, 480);
	}
public slots:
	void clear()
	{
		auto iter = m_overlays.begin();
		while (iter != m_overlays.end()) {

			scene()->removeItem(iter.key());
			m_overlays.remove(iter.key());
			delete iter.key();
			iter++;
		}
	}
	void setLookPosition(int x, int y)
	{

		auto xForMap = (float(x) / float(1920)) * 910 + 35;
		auto yForMap = (float(y) / float(1080)) * 560 + 35;
		auto point = QPointF(xForMap/2, yForMap/2);// mapFromScene(xForMap, yForMap);
		addPointAndClearOtherPoints(point, QColor("white"));

	}
	void AddLookPosition(int x, int y)
	{

		auto xForMap = (float(x) / float(1920)) * 910 + 35;
		auto yForMap = (float(y) / float(1080)) * 560 + 35;
		auto point = QPointF(xForMap / 2, yForMap / 2);// mapFromScene(xForMap, yForMap);
		addPointAA(point, QColor("yellow"));

	}
	void setText(const QString & str)
	{
		auto xForMap = (float(960) / float(1920)) * 910 + 35;
		auto yForMap = (float(540) / float(1080)) * 560 + 35;
		auto point = QPointF(xForMap / 2, yForMap / 2);// mapFromScene(xForMap, yForMap);
		addPointAndClearOtherPoints(point, QColor("red"));
	}
	void setPosition(int x, int y)
	{
		auto xForMap = (float(x) / float(1920)) * 910 + 35;
		auto yForMap = (float(y) / float(1080)) * 560 + 35;
		auto point = QPointF(xForMap/2, yForMap/2);// mapFromScene(xForMap, yForMap);
		addPoint(point, QColor("red"));

	}
	void setPositionQPoints(QVector<QPointF> qPoints) 
	{
		QColor colors[] = { "white" , "black" , "red" };
		QString strs[] = { "F" , "GL" , "GR" };
		for (auto const& qP : qPoints) {
			auto xForMap = (float(qP.x()) / float(1920)) * 910 + 35;
			auto yForMap = (float(qP.y()) / float(1080)) * 560 + 35;
			auto point = QPointF(xForMap / 2, yForMap / 2);
			static int c = 0;
			addPoint(point, QColor(colors[c%3]), strs[c%3]);
			c++;
		}
	}
	void setPixmap(QPixmap p) {
		if (m_backgroundImage != nullptr) {
			m_backgroundImage->setParentItem(nullptr);
			delete m_backgroundImage;
			m_backgroundImage = nullptr;
		}
		if (p.isNull()) {
			return;
		}
		m_backgroundImage = scene()->addPixmap(p);
		m_backgroundImage->setFlag(QGraphicsItem::ItemIsMovable, true);
		//setSceneRect(QRectF(4 * -p.width(), 4 * -p.height(), 8 * p.width(), 8 * p.height()));
		scale(1.0, 1.0);
	}
private:

	QMap<QGraphicsItem*, QString> m_overlays;
	QMap<QString, QGraphicsItem*> m_uniqueIdToOverlayMapper;

	QGraphicsPixmapItem* m_backgroundImage;
};