#pragma once
#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "SingleEyeTrackingInfo.h"

class DBManager : public QObject
{
public:
	DBManager(QObject *parent = 0);
	~DBManager();
public:
	bool deleteAll(); 
	bool addEyeTrackingInfo(const SingleEyeTrackingInfo & singleEyeTrackingInfo);
	std::vector<SingleEyeTrackingInfo> getAvailableEyeTrackingData();
private:
	QSqlDatabase * m_db;
	QSqlQuery    m_query;
};
#endif // !DBMANAGER_H