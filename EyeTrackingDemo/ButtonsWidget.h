#pragma once
#include <QWidget>
#include <QPushbutton>
#include <QVector>
#include <QLabel>
class ButtonsWidget : public QWidget
{
	Q_OBJECT
public:
	ButtonsWidget(QWidget* parent = nullptr);
private:
	void createMembers();
	void makeConnection();
	void setupLayouts();
public slots:
	void onDump();
	void onAddNewData();
	void onStopToAddNewData();
	void onUpdateAvailableData(int screenNumber, int value);
signals:
	void solve();
private:
	QPushButton* m_dumpButton;
	QPushButton* m_solveButton;
	QPushButton* m_addNewData;
	QPushButton* m_stopToAddNewData;
	std::map<int, QLabel*> m_labels;
};