#pragma once

#include "EyeTrackingInfoDispatcher.h"
#include <unordered_map>
#include <vector>
enum Axis
{
	eX = 0,
	eY = 1,
	eZ = 2
};
struct KnownVariables
{
	float headPositionX;
	float headPositionY;
	float headPositionZ;
	float headForwardX;
	float headForwardY;
	float headForwardZ;
	float coordinateX;
	float coordinateY;

	std::vector<float> getRow(const Axis & axis,int variablePosition, int countOfPoints) const ;
	std::vector<float> getHeadPosition() const ;
};

struct UnknownVariables
{
	float screenPositionX;
	float screenPositionY;
	float screenPositionZ;
	float screenLeftX;
	float screenLeftY;
	float screenLeftZ;
	float screenUpX;
	float screenUpY;
	float screenUpZ;
	std::vector<float> distance;
};

class ScreenPositionRecoverer
{
public:
	ScreenPositionRecoverer();
#ifndef SINGLE_SCREEN
	void initialize(EyeFeatureToPointMappingVector eyeFeayuresAndPointsVector);
#else
	void initialize(EyeFeatureToPointMapping eyeFeayuresAndPoints);
#endif
	//testFunctions
	void addPoint(int x ,int y)
	{
		POINT pt;
		pt.x = x;
		pt.y = y;
		m_points.at(0).push_back(pt);
	}

	POINT getNeareastPointByAproximation(const cv::Vec3f& point, const std::vector<cv::Vec3f>& points, int indexOfScreen);
	std::vector<POINT> getPointsForScreen(const EyeFeature& eyeFeature, int indexOfScreen, int & countOfMatchings);

	int getScreensCount();                                 
	cv::Mat solve(int indexOfScreen);
	bool isInScreen(const EyeFeature& eyeFeature, std::vector<POINT>& ptOut,int indexOfScreen);
	std::vector<POINT>  getPossiblePointByAproximation(const EyeFeature& eyeFeature, int indexOfScreen);
	std::pair<float, float > getPossiblePoint(const EyeFeature& eyeFeature, int indexOfScreen);
	void printMat(int index);
	std::vector<cv::Mat> getPossibleMatrixes();
	//std::vector<cv::Mat> getPossibleEqualMatrixes();
	cv::Mat createMat(cv::Mat& _mat , int index);
	cv::Mat matEqualTo(int index);
	void writeResults(const std::string & str,int index);
private:
	void addVariables(EyeFeatureToPointMapping eyeFeatureToPointMapping, int indexOfScreen,  int positionInVector, int indexOfVariable);
	std::vector<cv::Vec3f> getAllPoints(float & minX , float & maxX , float & minY , float & maxY, float & minZ , float & maxZ,int indexOfScreen);
	POINT getNearestPoint(const cv::Vec3f & point , const std::vector<cv::Vec3f>& points, int indexOfScreen);
	std::vector<POINT> getNeareastPointByAproximationAndGazeOrientation(const cv::Vec3f& point, const std::vector<std::vector<cv::Vec3f>>& points, int indexOfScreen);
	cv::Mat createMat(const std::vector<KnownVariables> & variables);
	cv::Mat matEqualTo(const std::vector<KnownVariables> & variables);
	std::pair<float, float> getNormalizedScreenCoordinates(const POINT & pt);
private:
	std::vector<std::vector<KnownVariables>> m_variables;
	std::vector<std::vector<POINT>> m_points;
	std::vector<UnknownVariables> m_unknownVariables;
	cv::Vec3b m_lastPossiblePoint;

};