#pragma once
#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"

#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>


class EyeFeature
{
public:
	EyeFeature(const cv::Vec6d & PosTransAndRotation, const cv::Point3f & gaze1, const cv::Point3f & gaze2);
	EyeFeature(const std::string & feature);
	EyeFeature();
	void setId(int id);
	int getId() const;
	void normalaizeFeature(const std::vector<double> & vec);
	double distance(const EyeFeature & eyeFeature) const;
	std::vector<double> getFeature() const;
	operator std::string();
	bool operator< (const EyeFeature & other) const;
	friend std::ostream& operator<<(std::ostream& out, const EyeFeature& eF);
private:
	std::vector<double> m_feature;
	int m_id;
};