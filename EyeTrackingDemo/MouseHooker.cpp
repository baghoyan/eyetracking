#include "MouseHooker.h"

static MouseListenerPtr s_mouseListener = nullptr;

HHOOK mouseHook = 0;


BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
    int* Count = (int*)dwData;
    s_mouseListener->appendScreen(lprcMonitor->left, lprcMonitor->right, lprcMonitor->bottom, lprcMonitor->top, *Count);
    (*Count)++;
    return TRUE;
}

LRESULT MouseHookCallback(int nCode, WPARAM wParam, LPARAM lParam)
{
    static bool Tracking = false;
    if (nCode >= 0) {

        POINT p;
        switch (wParam)
        {
        case WM_LBUTTONDOWN:
            //std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
            GetCursorPos(&p);
            s_mouseListener->clickedOn(p.x, p.y);
            break;
        }
    }
    return CallNextHookEx(mouseHook, nCode, wParam, lParam);
}

void initialize()
{
    auto hook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookCallback, NULL, 0);
    //m_lastMousePosition = POINT();
    MSG msg;
    WNDCLASS wc = { };
    wc.style = CS_DBLCLKS;
    RegisterClass(&wc);

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    //std::thread t(&MouseListener::setWinEventHookForMouse, WH_MOUSE_LL);
    //t.detach();
}

MouseListenerPtr MouseListener::getInstance()
{
    if (s_mouseListener == nullptr) {
        s_mouseListener = new MouseListener;
    }
    return s_mouseListener;
}

int MouseListener::MonitorCount()
{
    static bool append = true;
    if (append) {
        int Count = 0;
        if (EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM)&Count))
            return Count;
        append = false;
    }
    return m_screenSizes.size();//signals an error
}

void MouseListener::appendScreen(long left, long right, long bottom, long top, long numberOfScreen)
{
    m_screenSizes.emplace_back(left, right, bottom, top, numberOfScreen);
}

bool WindowSize::getPointOnScreen(const POINT& p, int& xOut, int& yOut)
{
    return getPointOnScreen(p.x,p.y,xOut,yOut);
}

bool WindowSize::getPointOnScreen(int xIn, int yIn, int& xOut, int& yOut)
{
    if (isPointInScreen(xIn, yIn)) {
        xOut = xIn - left_;
        yOut = yIn - top_;
        return true;
    }
    else {
        return false;
    }
}

bool WindowSize::isPointInScreen(const POINT& p)
{
    return isPointInScreen(p.x,p.y);
}

bool WindowSize::isPointInScreen(int x, int y)
{
    std::cout <<"right_ " <<right_ <<   " x " << x << " left_ " << left_<<std::endl;
    std::cout <<"top_ " <<top_ <<   " y " << y << " bottom_ " <<  bottom_<<std::endl;
    if (x <= right_ && x >= left_ && y >= top_ && y <= bottom_) {
        return true;
    }
    return false;
}
