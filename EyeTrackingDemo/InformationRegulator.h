#pragma once

#include <qobject.h>
#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"
#include <vector>
#include <Qpoint>
#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>
#include <QSlider>
class InformationRegulator : public QObject
{
	Q_OBJECT
public:
	InformationRegulator(); 
public:

	double attractionThreshold() const
	{
		return m_attractionThreshold / 10;
	}

public slots:
	void addNewPose(const QPointF& pt, const cv::Vec6d& vec6);
	void onNewPose(cv::Vec6d vec);
	void onSetThreshold(int t);
signals:
	void lookingTo(int x, int y);
	void failed(QString);
private:
	double distance(const cv::Vec6d& first, const cv::Vec6d& second);
private:
	std::vector<cv::Vec6d> m_poses;
	std::vector<QPointF> m_positions;
	double m_attractionThreshold;

};