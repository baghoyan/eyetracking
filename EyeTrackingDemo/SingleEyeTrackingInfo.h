#pragma once
#include <string>

#include "EyeFeature.h"

struct SingleEyeTrackingInfo {
public:
	int m_id;
	std::string m_featureVector;
	std::string m_mousePosition;
	bool m_isPositiveData;
	int m_screenNumber;
	EyeFeature getEyeFeature() const;
	POINT getPoint() const;
};
