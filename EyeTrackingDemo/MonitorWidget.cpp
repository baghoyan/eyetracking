#include "MonitorWidget.h"
#include <QHBoxLayout>
MonitorWidget::MonitorWidget(int countOfMonitors, QPixmap pixmap, QWidget* parent)
	:m_countOfMonitors(countOfMonitors)
	, m_pixmap(pixmap)
	, m_colorCollector(0)
{
	createMembers();
	setupLayouts();
	makeConnections();
}

void MonitorWidget::setGoodCasePixmap(const QPixmap& pix)
{
	m_goodCasePixmap = pix;
}

void MonitorWidget::setBadCasePixmap(const QPixmap& pix)
{
	m_badCasePixmap = pix;
}

void MonitorWidget::createMembers()
{
	for (auto i = 0; i < m_countOfMonitors; ++i) {
		auto currentMonitor = new SingleImageInfoViewer(); 
		m_singleMonitor.push_back(currentMonitor);
		currentMonitor->setPixmap(m_pixmap);
	}
}

void MonitorWidget::setupLayouts()
{
	QHBoxLayout* widgetLayout = new QHBoxLayout(this); 
	for (auto i = 0; i < m_singleMonitor.size(); ++i) {
		QVBoxLayout* currentVerticalLayout = new QVBoxLayout;

		currentVerticalLayout->addWidget(new QLabel("number " + QString::number(i + 1) + " screen"));
		currentVerticalLayout->addWidget(m_singleMonitor.at(i));
		widgetLayout->addLayout(currentVerticalLayout);
	}
}

void MonitorWidget::makeConnections()
{
}

void MonitorWidget::onDrawPointOnScreenMultiPoints(int screenNumber, QVector<QPointF> points)
{
	//screenNumber;
	if (screenNumber == -1) {
		m_colorCollector++;
		for (auto i = 0; i < m_singleMonitor.size(); ++i) {
			auto m = m_singleMonitor.at(i);
			m->clear();
			if (m_colorCollector > 5 && m_lastScreen == screenNumber) {
				m->setPixmap(m_badCasePixmap);
			}
		}
	}
	else {
		for (auto i = 0; i < m_singleMonitor.size(); ++i) {
			auto m = m_singleMonitor.at(i);
			m->clear();
			if (screenNumber != i) {
				m->setPixmap(m_badCasePixmap);
			}
		}
	}
	m_colorCollector = 0;
	if (screenNumber != -1) {
		m_singleMonitor.at(screenNumber)->setPixmap(m_goodCasePixmap);
		m_singleMonitor.at(screenNumber)->setPositionQPoints(points);
	}
}

void MonitorWidget::onDrawPointOnScreen(int screenNumber, int x, int y)
{
	//screenNumber /= 3;
	if (screenNumber == -1) {
		m_colorCollector++;
		for (auto i = 0; i < m_singleMonitor.size(); ++i) {
			auto m = m_singleMonitor.at(i);
			m->clear();
			if (m_colorCollector>2) {
				m->setPixmap(m_badCasePixmap);
			}
		}
	}
	//else {
	//	for (auto i = 0; i < m_singleMonitor.size(); ++i) {
	//		auto m = m_singleMonitor.at(i);
	//		m->clear();
	//		if (screenNumber!= i) {
	//			m->setPixmap(m_badCasePixmap);
	//		}
	//	}
	//}
	//if (screenNumber != -1) {
	//	m_colorCollector = 0;
	//	m_singleMonitor.at(screenNumber)->setPixmap(m_goodCasePixmap);
	//	m_singleMonitor.at(screenNumber)->setLookPosition(x, y);
	//}
}