#pragma once

#include <QPixmap>
#include <QPointF>
#include <qobject.h>
#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"
#include "ScreenPositionRecoverer.h"

#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>
class FaceLandmarkDetector : public QObject
{
	Q_OBJECT
public:
	FaceLandmarkDetector();
	void run();
	std::string getFeatureVector();  
	std::pair<cv::Vec6d, cv::Mat> getImage();
public slots:
	void onClicked(int x, int y);
	void onClickWithScreenNumber(int screenNumber, int x, int y);
	void onSolve();
	void detectLookingScreen();
private:
	std::string getFeatureVector(const cv::Vec6d & vec6d, const cv::Point3f & gaze1, const cv::Point3f & gaze2);
	std::string getFeatureVector(const cv::Vec6d & vec6d, const cv::Vec6d & gaze1, const cv::Vec6d & gaze2);
	std::string getMouseString(int x, int y);

signals:
	void lookingTo(int x, int y);
	void newImage(QPixmap img);
	void newHistory(QPointF, cv::Vec6d);
	void newPose(cv::Vec6d);
	void liveImage(QPixmap img);
	void drawOnScreen(int screenNumber, int x, int y);
	void drawOnScreenMultiPoints(int screenNumber, QVector<QPointF> points);
private:
	ScreenPositionRecoverer* m_screenPositionRecoverer;
	std::pair<cv::Vec6d, cv::Mat> m_vecAndImage;
	std::string m_lastFeature;
	std::mutex m_mutex;
	int m_lastScreen;
	bool m_startDetecting;
	bool m_viewOrientation;

	LandmarkDetector::FaceModelParameters m_det_parameters;
	LandmarkDetector::CLNF m_face_model; 
	Utilities::SequenceCapture m_sequence_reader;

};