#include "InformationRegulator.h"
#include <qdebug.h>
InformationRegulator::InformationRegulator()
{
	m_attractionThreshold = 200;
}

void InformationRegulator::onSetThreshold(int t)
{
	m_attractionThreshold = t * 10;
}
void InformationRegulator::addNewPose(const QPointF& pt, const cv::Vec6d& vec6)
{
	m_poses.push_back(vec6);
	m_positions.push_back(pt);
}
void InformationRegulator::onNewPose(cv::Vec6d vec)
{
	if (m_poses.size() < 4) {
		return;
	}
	auto min = std::numeric_limits<int>::max();
	int pos = -1;
	for (auto i = 0; i < m_poses.size(); ++i) {
		auto dist = distance(m_poses.at(i), vec);
		if (dist < min) {
			pos = i;
			min = dist;
		}
	}
	
	if (pos != -1 && min < m_attractionThreshold) {
		emit(lookingTo(m_positions.at(pos).x(), m_positions.at(pos).y()));
		emit(failed("looking at x: " +QString::number(m_positions.at(pos).x()) + ", y:" + QString::number(m_positions.at(pos).y()) + " Threshold is " + QString::number( m_attractionThreshold) )); 
	}
	else {
		emit(failed("Distracted : Threshold is " + QString::number( m_attractionThreshold)));
	}
}

double InformationRegulator::distance(const cv::Vec6d& first, const cv::Vec6d& second)
{
	double res = 0.0;
	for (auto i = 0; i < 6; ++i) {
		res += (first[i] - second[i]) * (first[i] - second[i]);
	}
	return res;
}
