#include "ButtonsWidget.h"
#include "DBManager.h"
#include "EyeTrackingInfoDispatcher.h"
#include "MouseHooker.h"
#include <QHBoxLayout>
ButtonsWidget::ButtonsWidget(QWidget* parent)
	:QWidget(parent)
{
	createMembers();
	setupLayouts();
	makeConnection();
	
}

void ButtonsWidget::createMembers()
{
	m_solveButton = new QPushButton("Recover Screen Position with current data");
	m_addNewData = new QPushButton("Add new data");
	m_stopToAddNewData = new QPushButton("Stop to add new data");
	m_dumpButton = new QPushButton("Delete available data");
	for (auto i = 0; i < MouseListener::getInstance()->MonitorCount(); ++i) {
		m_labels[i]=new QLabel("Available Data for screen N:" + QString::number(i)+ " -> 0");
	}
}

void ButtonsWidget::makeConnection()
{
	connect(m_dumpButton, &QPushButton::clicked, this, &ButtonsWidget::onDump);
	connect(m_addNewData, &QPushButton::clicked, this, &ButtonsWidget::onAddNewData);
	connect(m_stopToAddNewData, &QPushButton::clicked, this, &ButtonsWidget::onStopToAddNewData);
	connect(m_solveButton, &QPushButton::clicked, this, &ButtonsWidget::solve);
}

void ButtonsWidget::setupLayouts()
{
	QVBoxLayout* hBoxLayout = new QVBoxLayout(this);
	hBoxLayout->addWidget(m_addNewData,0,Qt::AlignTop);
	hBoxLayout->addWidget(m_stopToAddNewData, 0 , Qt::AlignTop);
	hBoxLayout->addWidget(m_solveButton, 0 , Qt::AlignTop);
	hBoxLayout->addWidget(m_dumpButton,0, Qt::AlignTop);

	for (auto m : m_labels) {
		static int strech = 0;
		static int count = 0;
		count++;
		if (count == m_labels.size()) {
			strech = 1;
		}
		hBoxLayout->addWidget(m.second, strech, Qt::AlignTop);
	}
}

void ButtonsWidget::onDump()
{
	DBManager* dbManager = new DBManager;
	dbManager->deleteAll();
	delete dbManager;
	dbManager = nullptr;
}

void ButtonsWidget::onAddNewData()
{
	EyeTrackingInfoDispatcher::getInstance()->setAddingState(true);
}

void ButtonsWidget::onStopToAddNewData()
{
	EyeTrackingInfoDispatcher::getInstance()->setAddingState(false);
}

void ButtonsWidget::onUpdateAvailableData(int screenNumber, int value)
{
	qDebug() << __FUNCTION__ << " : " << __LINE__ ;
	std::cout << "Screen Number " << screenNumber  << " " << m_labels.size() << std::endl;
	if (m_labels.find(screenNumber) != m_labels.end()) {
		m_labels[screenNumber]->setText("Available Data for screen N:" + QString::number(screenNumber) + " -> " + QString::number(value));
	}
}
