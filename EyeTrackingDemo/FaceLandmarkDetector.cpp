#include "FaceLandmarkDetector.h"

#include <QImage>
#include <QPixmap>
#include <qpoint.h>
#include "EyeTrackingInfoDispatcher.h"

#include "ScreenPositionRecoverer.h"
static void printErrorAndAbort(const std::string& error)
{
	std::cout << error << std::endl;
	abort();
}

#define FATAL_STREAM( stream ) \
printErrorAndAbort( std::string( "Fatal error: " ) + stream )

std::vector<std::string> get_arguments(int argc, char** argv)
{

	std::vector<std::string> arguments;

	for (int i = 0; i < argc; ++i)
	{
		arguments.push_back(std::string(argv[i]));
	}
	return arguments;
}

template <class T>
std::pair<T, T > getEyeyPosition(const std::vector<T>& points) {
	float avgXPoints = 0.f;
	for (auto const& pt : points) {
		avgXPoints += pt.x;
	}
	avgXPoints /= points.size();

	T leftEyeCoordinates, rightEyeCoordinates;
	int leftEyePointsCount = 0, rightEyePointsCount = 0;
	for (auto const& pt : points) {
		if (pt.x < avgXPoints) {
			leftEyeCoordinates += pt;
			leftEyePointsCount++;
		}
		else {
			rightEyeCoordinates += pt;
			rightEyePointsCount++;
		}
	}
	leftEyeCoordinates /= leftEyePointsCount;
	rightEyeCoordinates /= rightEyePointsCount;

	return { leftEyeCoordinates,rightEyeCoordinates };
}

FaceLandmarkDetector::FaceLandmarkDetector()
{
	m_lastScreen = -1;
	m_screenPositionRecoverer = nullptr;
	m_startDetecting = false;
	m_face_model = LandmarkDetector::CLNF(m_det_parameters.model_location);
	if (!m_sequence_reader.OpenWebcam(0)) {
		exit(-1);
	}
}
void FaceLandmarkDetector::onClicked(int x, int y)
{
	
	auto pix = QPixmap::fromImage(QImage((unsigned char*)m_vecAndImage.second.data, m_vecAndImage.second.cols, m_vecAndImage.second.rows, QImage::Format_RGB888).rgbSwapped());

	QPointF pf(x, y);
	SingleEyeTrackingInfo singleEyeTrackingInfo;
	singleEyeTrackingInfo.m_featureVector = m_lastFeature;
	singleEyeTrackingInfo.m_mousePosition = getMouseString(x, y);
	singleEyeTrackingInfo.m_isPositiveData = true;
	EyeTrackingInfoDispatcher::getInstance()->appendNewInfo(singleEyeTrackingInfo);
	emit(newImage(pix));
}
void FaceLandmarkDetector::onClickWithScreenNumber(int screenNumber, int x, int y)
{
	//m_viewOrientation = true;
	QPointF pf(x, y);
	SingleEyeTrackingInfo singleEyeTrackingInfo;
	singleEyeTrackingInfo.m_featureVector = getFeatureVector();
	singleEyeTrackingInfo.m_mousePosition = getMouseString(x, y);
	singleEyeTrackingInfo.m_isPositiveData = true;
	singleEyeTrackingInfo.m_screenNumber = screenNumber;
	EyeTrackingInfoDispatcher::getInstance()->appendNewInfo(singleEyeTrackingInfo);
	emit(newHistory(pf, m_vecAndImage.first));
}
void FaceLandmarkDetector::onSolve()
{
	m_startDetecting = false;
	std::this_thread::sleep_for(std::chrono::seconds(1));
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	if (m_screenPositionRecoverer != nullptr) {
		delete m_screenPositionRecoverer;
	}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_screenPositionRecoverer = new ScreenPositionRecoverer;
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_screenPositionRecoverer->initialize(EyeTrackingInfoDispatcher::getInstance()->getEyeFeaturesAndPoints());
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	for (auto i = 0; i < m_screenPositionRecoverer->getScreensCount(); ++i) {
		std::cout << "Size OF Screen  " << i / 3 << "Vector " << i % 3 << std::endl;
		m_screenPositionRecoverer->solve(i);
		m_screenPositionRecoverer->writeResults("result" + std::to_string(i)+".txt", i);
	}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_startDetecting = true;
}
void FaceLandmarkDetector::detectLookingScreen()
{
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	if (m_screenPositionRecoverer != nullptr) {
		delete m_screenPositionRecoverer;
	}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_screenPositionRecoverer = new ScreenPositionRecoverer;
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_screenPositionRecoverer->initialize(EyeTrackingInfoDispatcher::getInstance()->getEyeFeaturesAndPoints());
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	for (auto i = 0; i < m_screenPositionRecoverer->getScreensCount(); ++i) {
		m_screenPositionRecoverer->solve(i);
		//m_screenPositionRecoverer->writeResults("result" + std::to_string(i) + ".txt", i);
	}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	m_startDetecting = true;
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
	if (m_screenPositionRecoverer != nullptr && m_startDetecting) {

	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
		bool inScreen = false;
		std::vector<POINT> lookingToPoint;
		std::vector<std::vector<POINT>> lM(m_screenPositionRecoverer->getScreensCount()/3);
		auto currentFeature = getFeatureVector();
		int screenNumber = -1;
		int lastScreenMatchingsCount = -1;

	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
		for (auto i =0 ; i < m_screenPositionRecoverer->getScreensCount()/3; ++i)
		{
			int countOfMatchings = 0; 
			auto pt = m_screenPositionRecoverer->getPointsForScreen(EyeFeature(currentFeature), i, countOfMatchings);
			if (countOfMatchings == 0) {
				continue;
			}
			else if (lastScreenMatchingsCount == countOfMatchings ) {
				if (m_lastScreen == i) {
					inScreen = true;
					lookingToPoint = pt;
					screenNumber = i;
					lastScreenMatchingsCount = countOfMatchings;
				}
			}
			else {
				inScreen = true;
				lookingToPoint = pt;
				screenNumber = i;
				lastScreenMatchingsCount = countOfMatchings;
			}
		}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
		if (!inScreen) {
			emit(drawOnScreen(-1, -1, -1));
		}
		else {
			QVector<QPointF> lookingToPointQPoints;
			for (auto const& lTP : lookingToPoint) {
				lookingToPointQPoints.push_back(QPointF(lTP.x, lTP.y));
			}
			m_lastScreen = screenNumber;
			emit(drawOnScreenMultiPoints(screenNumber, lookingToPointQPoints));
		}
	std::cout << __FUNCTION__ << " : " << __LINE__ << std::endl;
		//for (auto i = 0; i < m_screenPositionRecoverer->getScreensCount(); ++i) {
		//	if (m_screenPositionRecoverer->isInScreen(EyeFeature(currentFeature), lookingToPoint, i)) {
		//		std::cout << i << std::endl;
		//		std::cout << i/3 << std::endl;
		//		inScreen = true;
		//		lM.at(i / 3).push_back(lookingToPoint.front());
		//		//break;
		//	}
		//}
		//std::cout << "___________________" << std::endl;
		//if (!inScreen) {
		//	emit(drawOnScreen(-1, -1, -1));
		//}
		//else {
		//	QVector<QPointF> lookingToPointQPoints;
		//	
		//	auto screenPosition = int();
		//	auto matchingSize = 0;
		//	
		//	for (auto i = 0; i < lM.size(); ++i) {
		//		if (lM.at(i).size() > matchingSize) {
		//			matchingSize = lM.at(i).size();
		//			screenPosition = i;
		//			//for (auto const& lTP : lM.at(i)) {
		//			//	lookingToPointQPoints.push_back(QPointF(lTP.x, lTP.y));
		//			//}
		//		}
		//		else if (lM.at(i).size() == matchingSize) {
		//			if (i == m_lastScreen) {
		//				screenPosition = m_lastScreen;
		//			}
		//		}
		//	}
		//	for (auto const& lTP : lM.at(screenPosition)) {
		//		lookingToPointQPoints.push_back(QPointF(lTP.x, lTP.y));
		//	}
		//	m_lastScreen = screenPosition;
		//	emit(drawOnScreenMultiPoints(screenPosition, lookingToPointQPoints));
		//}
	}
}
std::string FaceLandmarkDetector::getFeatureVector(const cv::Vec6d & vec6d, const cv::Point3f & gaze1, const cv::Point3f & gaze2)
{
	std::string res; 
	for (auto i = 0;  i < 6; ++i) {
		res += std::to_string(vec6d[i]) + " ";
	}
	res += std::to_string(gaze1.x) + " " + std::to_string(gaze1.y) + " " + std::to_string(gaze1.z) + " ";
	res += std::to_string(gaze2.x) + " " + std::to_string(gaze2.y) + " " + std::to_string(gaze2.z) + " ";
	return res;
}
std::string FaceLandmarkDetector::getFeatureVector(const cv::Vec6d& vec6d, const cv::Vec6d& gaze1, const cv::Vec6d& gaze2) 
{

	std::string res;
	for (auto i = 0; i < 6; ++i) {
		res += std::to_string(vec6d[i]) + " ";
	}
	for (auto i = 0; i < 6; ++i) {
		res += std::to_string(gaze1[i]) + " ";
	}
	for (auto i = 0; i < 6; ++i) {
		res += std::to_string(gaze2[i]) + " ";
	}
	return res;
}
std::string FaceLandmarkDetector::getMouseString(int x, int y)
{
	return std::to_string(x) + " " + std::to_string(y);
}
void FaceLandmarkDetector::run()
{
	std::vector<std::string> arguments = {"-f" , "C:/Users/levon/Pictures/Camera Roll/WIN_20200703_19_43_12_Pro.mp4" };

	// no arguments: output usage
	//if (arguments.size() == 1)
	//{
	//	std::cout << "For command line arguments see:" << std::endl;
	//	std::cout << " https://github.com/TadasBaltrusaitis/OpenFace/wiki/Command-line-arguments";
	//	return 0;
	//}
	LandmarkDetector::FaceModelParameters det_parameters;


	// The modules that are being used for tracking
	LandmarkDetector::CLNF face_model(det_parameters.model_location);
	if (!face_model.loaded_successfully)
	{
		std::cout << "ERROR: Could not load the landmark detector" << std::endl;
		return;
	}

	if (!face_model.eye_model)
	{
		std::cout << "WARNING: no eye model found" << std::endl;
	}

	// Open a sequence
	Utilities::SequenceCapture sequence_reader;

	// A utility for visualizing the results (show just the tracks)
	Utilities::Visualizer visualizer(true, false, false, false);

	// Tracking FPS for visualization
	Utilities::FpsTracker fps_tracker;
	fps_tracker.AddFrame();
	
	int sequence_number = 0;
	//cv::VideoCapture cap;
	//cap.open(0);
	m_viewOrientation = false;
	while (true) // this is not a for loop as we might also be reading from a webcam
	{
		
		if (!sequence_reader.OpenWebcam(0))
			break;
		cv::Mat rgb_image = sequence_reader.GetNextFrame();
		//cap >> rgb_image;
		while (!rgb_image.empty()) 
		{
			//if (!m_viewOrientation) {
			//	rgb_image = sequence_reader.GetNextFrame();
			//	continue;
			//}
			//else {
			//	m_viewOrientation = false;
			//}
			//std::this_thread::sleep_for(std::chrono::milliseconds(2000));
			// Reading the images
			cv::Mat_<uchar> grayscale_image = sequence_reader.GetGrayFrame();

			// The actual facial landmark detection / tracking
			bool detection_success = LandmarkDetector::DetectLandmarksInVideo(rgb_image, face_model, det_parameters, grayscale_image);

			// Gaze tracking, absolute gaze direction
			cv::Point3f gazeDirection0(0, 0, -1);
			cv::Point3f gazeDirection1(0, 0, -1);

			if (detection_success && face_model.eye_model)
			{
				GazeAnalysis::EstimateGaze(face_model, gazeDirection0, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, true);
				GazeAnalysis::EstimateGaze(face_model, gazeDirection1, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, false);
			}
			cv::Point3f forward,up , left;
			cv::Vec6d pose_estimate = LandmarkDetector::GetPoseWRTCamera(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, forward,up,left);
			
			fps_tracker.AddFrame();
			visualizer.SetImage(rgb_image, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
			visualizer.SetObservationLandmarks(face_model.detected_landmarks, face_model.detection_certainty, face_model.GetVisibilities());
			auto lDC3DEL = LandmarkDetector::Calculate3DEyeLandmarks(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
			auto lDCAEL = LandmarkDetector::CalculateAllEyeLandmarks(face_model);
			static int counter = 0;
			visualizer.SetObservationGaze(gazeDirection0, gazeDirection1, LandmarkDetector::CalculateAllEyeLandmarks(face_model), LandmarkDetector::Calculate3DEyeLandmarks(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy), face_model.detection_certainty);
			auto gazePoss = getEyeyPosition(lDC3DEL);
			//visualizer.SetObservationGaze(gazeDirection0,gazeDirection1, LandmarkDetector::CalculateAllEyeLandmarks(face_model), LandmarkDetector::Calculate3DEyeLandmarks(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy), face_model.detection_certainty);
			visualizer.SetAxis((lDC3DEL.back()+ lDC3DEL.front())/2, up,left,-forward);
			visualizer.SetFps(fps_tracker.GetFPS());
			//char character_press = visualizer.ShowObservation();
			//newFeature.val[0] = gazePoss.first.x;
			//newFeature.val[1] = gazePoss.first.y;
			//newFeature.val[2] = gazePoss.first.z;
			//newFeature.val[3] = gazeDirection0.x;
			//newFeature.val[4] = gazeDirection0.y;
			//newFeature.val[5] = gazeDirection0.z;
			cv::Vec6d gazeFeature1;
			gazeFeature1.val[0] = gazePoss.first.x;
			gazeFeature1.val[1] = gazePoss.first.y;
			gazeFeature1.val[2] = gazePoss.first.z;
			gazeFeature1.val[3] = gazeDirection0.x;
			gazeFeature1.val[4] = gazeDirection0.y;
			gazeFeature1.val[5] = gazeDirection0.z;

			cv::Vec6d gazeFeature2;
			gazeFeature2.val[0] = gazePoss.second.x;
			gazeFeature2.val[1] = gazePoss.second.y;
			gazeFeature2.val[2] = gazePoss.second.z;
			gazeFeature2.val[3] = gazeDirection1.x;
			gazeFeature2.val[4] = gazeDirection1.y;
			gazeFeature2.val[5] = gazeDirection1.z;

			cv::Vec6d newFeature;
			newFeature.val[0] = pose_estimate.val[0];
			newFeature.val[1] = pose_estimate.val[1];
			newFeature.val[2] = pose_estimate.val[2];
			newFeature.val[3] = -forward.x;
			newFeature.val[4] = -forward.y;
			newFeature.val[5] = -forward.z;
			m_vecAndImage = { pose_estimate,visualizer.GetVisImage()};
			m_lastFeature=getFeatureVector(newFeature, gazeFeature1, gazeFeature2);
			//EyeTrackingInfoDispatcher::getInstance()->testFeature(EyeFeature(m_lastFeature));
			//SingleEyeTrackingInfo currentEyeTrackingInfo;
			//currentEyeTrackingInfo.m_featureVector = m_lastFeature;
			//currentEyeTrackingInfo.m_isPositiveData = false;
			//EyeTrackingInfoDispatcher::getInstance()->appendNewInfo(currentEyeTrackingInfo);
			if (m_screenPositionRecoverer != nullptr && m_startDetecting) {

				bool inScreen = false;
				std::vector<POINT> lookingToPoint; 
				for (auto i = 0; i <m_screenPositionRecoverer->getScreensCount()/3; ++i) {
					
					if (m_screenPositionRecoverer->isInScreen(EyeFeature(m_lastFeature), lookingToPoint, i)) {

						inScreen = true;
						std::cout << "inScreen ++++ " << inScreen  << std::endl;
						std::cout << "watching on: " << i  << std::endl;
						QVector<QPointF> lookingToPointQPoints;
						for (auto const& lTP : lookingToPoint) {
							lookingToPointQPoints.push_back(QPointF(lTP.x, lTP.y));
						}
						emit(drawOnScreenMultiPoints(i, lookingToPointQPoints));
						std::cout << "Emited Screend draw function : " << std::endl;
						break;
					}
				}
				std::cout << "inScreen " << inScreen << std::endl;
				if (!inScreen) {
					emit(drawOnScreen(-1, -1, -1));
				}
			}
			auto pix = QPixmap::fromImage(QImage((unsigned char*)m_vecAndImage.second.data, m_vecAndImage.second.cols, m_vecAndImage.second.rows, QImage::Format_RGB888).rgbSwapped());
			emit(newPose(pose_estimate));
			emit(liveImage(pix));
			// restart the tracker
			//if (character_press == 'r')
			//{
			//	face_model.Reset();
			//}
			//// quit the application
			//else if (character_press == 'q')
			//{
			//	return;
			//}

			// Grabbing the next frame in the sequence
			rgb_image = sequence_reader.GetNextFrame();
			//cap >> rgb_image;
		}

		// Reset the model, for the next video
		face_model.Reset();
		sequence_reader.Close();

		sequence_number++;

	}
}

std::string FaceLandmarkDetector::getFeatureVector()
{
	if (!m_face_model.loaded_successfully)
	{
		std::cout << "ERROR: Could not load the landmark detector" << std::endl;
		return std::string();
	}

	if (!m_face_model.eye_model)
	{
		std::cout << "WARNING: no eye model found" << std::endl;
	}
	int sequence_number = 0;
	Utilities::Visualizer visualizer(true, false, false, false);
	Utilities::FpsTracker fps_tracker;
	fps_tracker.AddFrame();
	while (true) 
	{
		cv::Mat rgb_image = m_sequence_reader.GetNextFrame();
		while (!rgb_image.empty())
		{
			cv::Mat_<uchar> grayscale_image = m_sequence_reader.GetGrayFrame();
			bool detection_success = LandmarkDetector::DetectLandmarksInVideo(rgb_image, m_face_model, m_det_parameters, grayscale_image);

			cv::Point3f gazeDirection0(0, 0, -1);
			cv::Point3f gazeDirection1(0, 0, -1);

			if (detection_success && m_face_model.eye_model)
			{
				GazeAnalysis::EstimateGaze(m_face_model, gazeDirection0, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy, true);
				GazeAnalysis::EstimateGaze(m_face_model, gazeDirection1, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy, false);
			}
			cv::Point3f forward, up, left;
			cv::Vec6d pose_estimate = LandmarkDetector::GetPoseWRTCamera(m_face_model, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy, forward, up, left);

			auto lDC3DEL = LandmarkDetector::Calculate3DEyeLandmarks(m_face_model, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy);
			auto gazePoss = getEyeyPosition(lDC3DEL);

			//gaze vector
			cv::Vec6d gazeFeature1;
			gazeFeature1.val[0] = gazePoss.first.x;
			gazeFeature1.val[1] = gazePoss.first.y;
			gazeFeature1.val[2] = gazePoss.first.z;
			gazeFeature1.val[3] = gazeDirection0.x;
			gazeFeature1.val[4] = gazeDirection0.y;
			gazeFeature1.val[5] = gazeDirection0.z;

			//gaze vector
			cv::Vec6d gazeFeature2;
			gazeFeature2.val[0] = gazePoss.second.x;
			gazeFeature2.val[1] = gazePoss.second.y;
			gazeFeature2.val[2] = gazePoss.second.z;
			gazeFeature2.val[3] = gazeDirection1.x;
			gazeFeature2.val[4] = gazeDirection1.y;
			gazeFeature2.val[5] = gazeDirection1.z;

			//head vector
			cv::Vec6d newFeature;
			newFeature.val[0] = pose_estimate.val[0];
			newFeature.val[1] = pose_estimate.val[1];
			newFeature.val[2] = pose_estimate.val[2];
			newFeature.val[3] = -forward.x;
			newFeature.val[4] = -forward.y;
			newFeature.val[5] = -forward.z;
			//cv::imshow("RGB ", rgb_image);
			visualizer.SetImage(rgb_image, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy);
			visualizer.SetObservationLandmarks(m_face_model.detected_landmarks, m_face_model.detection_certainty, m_face_model.GetVisibilities());
			visualizer.SetObservationGaze(gazeDirection0, gazeDirection1, LandmarkDetector::CalculateAllEyeLandmarks(m_face_model), LandmarkDetector::Calculate3DEyeLandmarks(m_face_model, m_sequence_reader.fx, m_sequence_reader.fy, m_sequence_reader.cx, m_sequence_reader.cy), m_face_model.detection_certainty);
			visualizer.SetFps(fps_tracker.GetFPS());
			//cv::imshow("VisImage " ,visualizer.GetVisImage());
			//cv::waitKey(0);
			return getFeatureVector(newFeature, gazeFeature1, gazeFeature2);
		}
	}

	return std::string();
}

std::pair<cv::Vec6d, cv::Mat> FaceLandmarkDetector::getImage()
{
	return m_vecAndImage;
}
