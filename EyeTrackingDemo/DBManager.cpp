#include "DBManager.h"
#include <QtSql/qsqlquery.h>
#include <QtCore/qdebug.h>
bool validAndOpenedDb(QSqlDatabase* argument)
{
	return (argument != NULL && argument->isOpen());
}


DBManager::DBManager(QObject *parent) :QObject(parent) {
	m_db = NULL;
	
	QString hostName, userName, password, databaseName;

	hostName = "127.0.0.1";
	userName = "root";
	databaseName = "eye_tracking_db.db";
	QSqlDatabase s_database;
	if (QSqlDatabase::isDriverAvailable("QSQLITE")) {
		qDebug() << "Driver is available ";
	}
	s_database = QSqlDatabase::addDatabase("QSQLITE", QString::number(rand()));
	//db = std::unique_ptr<QSqlDatabase>(new QSqlDatabase(db));
	//s_database.setHostName(hostName);
	//s_database.setUserName(userName);
	//s_database.setPassword(password);
	s_database.setDatabaseName(databaseName);

	if (!s_database.open()) {
		qDebug() << "DB isn't running ";
		qDebug() << s_database.lastError().text();
		qDebug() << s_database.lastError().databaseText();
		qDebug() << s_database.lastError().driverText();
		//	return false;
	}
	if (s_database.isValid()) {
	}
	if (s_database.isOpen()) {
	}
	m_db = new QSqlDatabase(s_database);
	m_query = QSqlQuery(*m_db);
}

DBManager::~DBManager() {
	m_query.finish();
	m_db->close();
	m_db = new QSqlDatabase();
	QSqlDatabase::removeDatabase(m_db->connectionName());
}

bool DBManager::deleteAll()
{
	if (validAndOpenedDb(m_db))
	{
		m_query.clear();
		if (m_query.exec("Delete FROM eye_tracking_info where id > 1"))
		{
			while (m_query.next())
			{
				return true;
			}
		}
		else {
			qDebug() << m_query.lastError();
			qDebug() << "We have a problem " << __FILE__ << ":" << __LINE__;
		}
	}
	return false;
}

bool DBManager::addEyeTrackingInfo(const SingleEyeTrackingInfo & singleEyeTrackingInfo)
{
	QString s = QString("INSERT INTO `eye_tracking_info` (`feature_vector`, `mouce_position`, `is_positive_data` , `screen_number`) VALUES(:id1,:id2,:id3,:id4);");
	QSqlQuery query(*m_db);
	query.prepare(s);
	query.bindValue(0, QString::fromStdString(singleEyeTrackingInfo.m_featureVector));
	query.bindValue(1, QString::fromStdString(singleEyeTrackingInfo.m_mousePosition));
	query.bindValue(2, singleEyeTrackingInfo.m_isPositiveData);
	query.bindValue(3, singleEyeTrackingInfo.m_screenNumber);
	if (validAndOpenedDb(m_db))
	{
		if (query.exec())
		{
			return true;
		}
		else {
			qDebug() << query.lastError();
			qDebug() << "We have a problem " << __FILE__ << ":" << __LINE__;
			return false;

		}
	}
	return false;
	return false;
}

std::vector<SingleEyeTrackingInfo> DBManager::getAvailableEyeTrackingData()
{
	qDebug() << __FUNCTION__ << " : " << __LINE__ ;
	std::vector<SingleEyeTrackingInfo> tmp;
	if (validAndOpenedDb(m_db))
	{
		m_query.clear();
		if (m_query.exec("SELECT * FROM eye_tracking_info;"))
		{
			while (m_query.next())
			{
				SingleEyeTrackingInfo currentEyeTrackingInfo;
				currentEyeTrackingInfo.m_featureVector = m_query.value("feature_vector").toString().toStdString();
				currentEyeTrackingInfo.m_mousePosition = m_query.value("mouce_position").toString().toStdString();
				currentEyeTrackingInfo.m_isPositiveData = m_query.value("is_positive_data").toInt();
				currentEyeTrackingInfo.m_screenNumber = m_query.value("screen_number").toInt();
				currentEyeTrackingInfo.m_id = m_query.value("id").toInt();
				tmp.push_back(currentEyeTrackingInfo);
			}
			return tmp;
		}
		else {
			qDebug() << m_query.lastError();
			qDebug() << "We have a problem " << __FILE__ << ":" << __LINE__;
		}
	}
	return tmp;
}
