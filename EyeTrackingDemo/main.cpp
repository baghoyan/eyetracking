#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSlider>

#include "MonitorWidget.h"
#include "ButtonsWidget.h"
#include <thread>

#include "MouseHooker.h"
#include <filesystem>
#include "FaceLandmarkDetector.h"
#include "InformationRegulator.h"
#include "DBManager.h"
#include "EyeTrackingInfoDispatcher.h"
#include "opencv2/highgui.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "EyeTrackingReporter.h"

#include <iostream>
using namespace cv;
using namespace std;

// static void help()
// {
//     cout << "\nThis program demonstrates kmeans clustering.\n"
//             "It generates an image with random points, then assigns a random number of cluster\n"
//             "centers and uses kmeans to move those cluster centers to their representitive location\n"
//             "Call\n"
//             "./kmeans\n" << endl;
// }
int msain(int argc, char* argv[])
{
	QApplication a(argc, argv);
	cv::Mat projectedPointsImage = cv::Mat(512, 512, CV_8UC3, cv::Scalar::all(255));

	int nReferenceCluster = 2;
	int nSamplesPerCluster = 238;

	int N = nReferenceCluster * nSamplesPerCluster; // number of samples
	int n = 12; // dimensionality of data

	std::string str = "41.7128 5.49103 466.957 -26.8408 7.8444 -0.572854 0.0318737 -0.217975 -0.970686 -0.0690507 -0.217343 -0.96642";
	EyeFeature feat(str);
	/*EyeTrackingInfoDispatcher::getInstance()->testNewFeature(str);
	std::cout << "After getMatOfPositiveEyeFeatures() function" << std::endl;
	return 5;*/
	int positiveFeaturesCount, negativeFeaturesCount;
	cv::Mat referenceCenters = EyeTrackingInfoDispatcher::getInstance()->getMatOfPositiveEyeFeatures(positiveFeaturesCount,negativeFeaturesCount);
	std::cout << "ReferenceCenters " << referenceCenters.size() << std::endl;
	cv::Mat points = cv::Mat::zeros(N, n, CV_32FC1);
	//cv::randu(points, cv::Scalar::all(-20), cv::Scalar::all(20)); // seed points around the center

	cv::Scalar clusterColor[] = {cv::Scalar(255,0,0),cv::Scalar(0,255,0)};
	for (int j = referenceCenters.rows-1 ;j >= 0; --j)
	{
		
		//cv::Mat & clusterCenter = referenceCenters.row(j);
		//for (int i = 0; i < nSamplesPerCluster; ++i)
		//{
			//// creating a sample randomly around the artificial cluster:
			//int index = j * nSamplesPerCluster + i;
			////samplesRow += clusterCenter;
			//for (int k = 0; k < points.cols; ++k)
			//{
			//	points.at<float>(index, k) += referenceCenters.at<float>(j, k);
			//}

			// projecting the 10 dimensional clusters to 2 dimensions:
			cv::circle(projectedPointsImage, cv::Point(referenceCenters.at<float>(j,0), referenceCenters.at<float>(j, 1)), 2, clusterColor[j>positiveFeaturesCount], -1);

		//}
	}


	cv::Mat labels; cv::Mat centers;
	std::vector<int> bestLabels(referenceCenters.rows,0);
	for (auto i = 0; i < positiveFeaturesCount; ++i) {
		bestLabels.at(i) = 1;
	}
	int k =2; 
	cv::kmeans(referenceCenters, k, bestLabels, cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 1000, 0.001), 100, cv::KMEANS_USE_INITIAL_LABELS, centers);
	std::cout << labels.size() << std::endl;
	std::cout << labels << std::endl;
	for (auto i = 0; i < bestLabels.size(); ++i) {
		std::cout << bestLabels.at(i) << std::endl;
	}
	for (int j = 0; j < centers.rows; ++j)
	{
		std::cout << centers.row(j) << std::endl;
		cv::circle(projectedPointsImage, cv::Point(centers.at<float>(j, 0), centers.at<float>(j, 1)), 5, cv::Scalar::all(0), 2);
	}

	cv::imshow("projected points", projectedPointsImage);
	cv::waitKey(0);
	return 0;
}
#include "ScreenPositionRecoverer.h"
std::ofstream of("alfa.beta");
void writeMat(cv::Mat mat) {
	for (auto i = 0; i < mat.rows; ++i) {
		for (auto j = 0; j < mat.cols; ++j) {
			of << mat.at<float>(i, j) << " ";
		}
	}
	of << "\n ";
}
int mainExperiement()
{
	ScreenPositionRecoverer* screenPositionRecoverer = new ScreenPositionRecoverer;
	screenPositionRecoverer->initialize(EyeTrackingInfoDispatcher::getInstance()->getEyeFeaturesAndPoints());
	screenPositionRecoverer->printMat(0);


	cv::Mat _mat(10, 10, CV_32FC1);

	screenPositionRecoverer->createMat(_mat,0);
	for (int i = 0; i < _mat.cols; ++i) {
		std::cout << "Mat [" << i << "]" << _mat.at<float>(0, i) << std::endl;
	}
	std::cout << " T  : " << _mat.size() << std::endl;
	cv::Mat _matTransposed = _mat.t();
	std::cout << " T  : " << _matTransposed.size() << std::endl;
	cv::Mat equalTo = screenPositionRecoverer->matEqualTo(0);
	//std::cout << "Determinant " << cv::determinant(_mat) << std::endl;
	//std::cout << "Mat inverse \n" << _mat.inv() << std::endl;
	//std::cout << "Equal TO " << equalTo << std::endl;
	//auto res = _mat.inv() * equalTo;
	//std::cout << "Result " << res;
	//writeMat(res);
	//writeMat(equalTo);
	//of.close();
	//return 4;
	//std::cout <<"First " << equalTo.size() << std::endl;
	//std::cout <<"Main mat : " << _mat.rows << " " << _mat.cols << std::endl;
	//cv::Mat dst;
	//cv::Mat matt = _mat.clone();
	//std::cout << "END" << std::endl;



	auto mtm = _mat.t() * _mat;
	std::cout << std::endl << mtm << std::endl;
	std::cout <<"Deter " << cv::determinant(mtm) << std::endl;
	std::cout << "Mat T * mat " << mtm.size() << std::endl;
	auto invMat = (_mat.t() * _mat).inv();

	std::cout << "inverse " << invMat.size() << std::endl;
	std::cout << "mat " << _mat.size() << std::endl;
	std::cout << "equalTo " << equalTo.size() << std::endl;
	cv::Mat dst = (_mat.t() * _mat).inv() * _mat.t() * equalTo;
	
	std::cout << "DST : " << dst << std::endl;
	return __LINE__;
	
	return 0;
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
	//ScreenPositionRecoverer* screenPositionRecoverer = new ScreenPositionRecoverer;
	//screenPositionRecoverer->addPoint(2,0);
	//screenPositionRecoverer->addPoint(-2,0);
	//screenPositionRecoverer->addPoint(0,2);
	//screenPositionRecoverer->addPoint(0,-2);
	//screenPositionRecoverer->addPoint(1,1);
	//screenPositionRecoverer->addPoint(1,-1);
	//screenPositionRecoverer->addPoint(-1,1);
	//screenPositionRecoverer->addPoint(-1,-1);
	//
	//std::vector<cv::Vec3f> points;
	//points.push_back({ 2,0,0 });
	//points.push_back({ -2,0,0 });
	//points.push_back({ 0,2,0 });
	//points.push_back({ 0,-2,0 });
	//points.push_back({ 2,0,2 });
	//points.push_back({ -2,0,2 });
	//points.push_back({ 0,2,2 });
	//points.push_back({ 0,-2,2 });
	//
	//auto pt = screenPositionRecoverer->getNeareastPointByAproximation(cv::Point3f(0,2, 2), points, 0);
	//std::cout << pt.x << " " << pt.y;
	//return a.exec();
	//mainExperiement();
	//return a.exec();
	
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<QVector<QPointF>>("QVector<QPointF>");
    qRegisterMetaType<QPointF>("pf");
    qRegisterMetaType<QPixmap>("pix");
    qRegisterMetaType<cv::Vec6d>("cv::Vec6d");
    std::thread t(&initialize);
    t.detach();
	QPixmap pixmap("Resources/1.png");
	pixmap = pixmap.scaled(pixmap.size() / 2);
	QPixmap pixmapGoodCase("Resources/1goodCase.png");
	pixmapGoodCase = pixmapGoodCase.scaled(pixmapGoodCase.size() / 2);
	QPixmap pixmapBadCase("Resources/1badCase.png");
	pixmapBadCase = pixmapBadCase.scaled(pixmapBadCase.size() / 2);
	MonitorWidget* monitorWidget = new MonitorWidget(MouseListener::getInstance()->MonitorCount(),pixmap);
	monitorWidget->setGoodCasePixmap(pixmapGoodCase);
	monitorWidget->setBadCasePixmap(pixmapBadCase);
	//monitorWidget->show();
	//QObject::connect(MouseListener::getInstance(), &MouseListener::clickOnScreen, monitorWidget, &MonitorWidget::onDrawPointOnScreen);
	//return a.exec();
	QThread* qThread = new QThread;
	EyeTrackingReporter* reporter = new EyeTrackingReporter(1000,qThread);
	qThread->start();
    SingleImageInfoViewer* monitorInfoViewer = new SingleImageInfoViewer;
    SingleImageInfoViewer* faceInfoViewer = new SingleImageInfoViewer;
    SingleImageInfoViewer* lokingInfoViewer = new SingleImageInfoViewer;
    SingleImageInfoViewer* live = new SingleImageInfoViewer;
    lokingInfoViewer->setPixmap(pixmap);
    FaceLandmarkDetector* faceLandmarkDetector = new FaceLandmarkDetector;
	QObject::connect(reporter, &EyeTrackingReporter::detectScreen, faceLandmarkDetector, &FaceLandmarkDetector::detectLookingScreen);
    InformationRegulator* informationRegulator = new InformationRegulator;
    //std::thread t1(&FaceLandmarkDetector::run, faceLandmarkDetector);
    //t1.detach();
	ButtonsWidget* buttonsWidget = new ButtonsWidget;
	QObject::connect(buttonsWidget, &ButtonsWidget::solve, faceLandmarkDetector, &FaceLandmarkDetector::onSolve);
    QObject::connect(MouseListener::getInstance(), &MouseListener::clicked, monitorInfoViewer, &SingleImageInfoViewer::setPosition);
    QObject::connect(MouseListener::getInstance(), &MouseListener::clickOnScreen, faceLandmarkDetector, &FaceLandmarkDetector::onClickWithScreenNumber);
    //QObject::connect(MouseListener::getInstance(), &MouseListener::clicked, faceLandmarkDetector, &FaceLandmarkDetector::onClicked);
    QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::newImage, faceInfoViewer, &SingleImageInfoViewer::setPixmap);
    QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::liveImage, live, &SingleImageInfoViewer::setPixmap);
	QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::drawOnScreen, monitorWidget, &MonitorWidget::onDrawPointOnScreen);
	QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::drawOnScreenMultiPoints, monitorWidget, &MonitorWidget::onDrawPointOnScreenMultiPoints);
    QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::newHistory, informationRegulator, &InformationRegulator::addNewPose);
    QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::newPose, informationRegulator, &InformationRegulator::onNewPose);
    QObject::connect(faceLandmarkDetector, &FaceLandmarkDetector::lookingTo, lokingInfoViewer, &SingleImageInfoViewer::setLookPosition);
    //QObject::connect(informationRegulator, &InformationRegulator::lookingTo, lokingInfoViewer, &SingleImageInfoViewer::setLookPosition);
    QObject::connect(EyeTrackingInfoDispatcher::getInstance(), &EyeTrackingInfoDispatcher::onScreen, lokingInfoViewer, &SingleImageInfoViewer::setLookPosition);
    QObject::connect(EyeTrackingInfoDispatcher::getInstance(), &EyeTrackingInfoDispatcher::notOnScreen, lokingInfoViewer, &SingleImageInfoViewer::setText);
    QObject::connect(EyeTrackingInfoDispatcher::getInstance(), &EyeTrackingInfoDispatcher::availablePoints, lokingInfoViewer, &SingleImageInfoViewer::AddLookPosition);
    QObject::connect(EyeTrackingInfoDispatcher::getInstance(), &EyeTrackingInfoDispatcher::availableDataChanged, buttonsWidget, &ButtonsWidget::onUpdateAvailableData);
	//EyeTrackingInfoDispatcher::getInstance()->showAllAvailablePositivePoints();
	EyeTrackingInfoDispatcherPtr ptr = EyeTrackingInfoDispatcher::getInstance();
	std::cout << "INSTANCE " << ptr << std::endl;
	EyeTrackingInfoDispatcher::getInstance()->setAddingState(true);
	QWidget* testWidget = new QWidget;
	QHBoxLayout* testWidgetLayout = new QHBoxLayout(testWidget);
	testWidgetLayout->addWidget(buttonsWidget);
	//testWidgetLayout->addWidget(lokingInfoViewer);
	testWidgetLayout->addWidget(live);
	testWidgetLayout->addWidget(monitorWidget);
	//buttonsWidget->show();
	//lokingInfoViewer->show();
	//live->show();
	ptr->showAllAvailablePositivePoints();
	testWidget->show();
	return a.exec();
    //QLabel* infoLabel = new QLabel;
	//
    //QObject::connect(informationRegulator, &InformationRegulator::failed, infoLabel, &QLabel::setText);
	//
    //QWidget* wid = new QWidget;
	//
    //QVBoxLayout* vBoxLay = new QVBoxLayout(wid);
    //QHBoxLayout* hlay = new QHBoxLayout();
    //QVBoxLayout* vlayLastImage = new QVBoxLayout;
    //vlayLastImage->addWidget(new QLabel("LastImage"));
    //faceInfoViewer->setMaximumWidth(270);
    //vlayLastImage->addWidget(faceInfoViewer);
    //
    //QVBoxLayout* vlaylive = new QVBoxLayout;
    //vlaylive->addWidget(new QLabel("Live"));
    //live->setMaximumWidth(270);
    //vlaylive->addWidget(live);
	//
	//
    //QVBoxLayout* vlayLooking = new QVBoxLayout;
    //vlayLooking->addWidget(new QLabel("Currently looking"));
    //lokingInfoViewer->setMaximumWidth(270);
    //vlayLooking->addWidget(lokingInfoViewer);
	//
    //monitorInfoViewer->setPixmap(pixmap);
    //QVBoxLayout* vlayMonitorInfoViewer = new QVBoxLayout;
    //vlayMonitorInfoViewer->addWidget(new QLabel("History of mouse clicked positions"));
    //monitorInfoViewer->setMaximumWidth(270);
    //vlayMonitorInfoViewer->addWidget(monitorInfoViewer);
    //hlay->addLayout(vlaylive);
    //hlay->addLayout(vlayLooking);
    //hlay->addLayout(vlayLastImage);
    //hlay->addLayout(vlayMonitorInfoViewer);
	//
    //QSlider* m_attractionThresholdSlider = new QSlider;
    //m_attractionThresholdSlider->setMaximum(100);
    //m_attractionThresholdSlider->setMinimum(0);
    //m_attractionThresholdSlider->setValue(informationRegulator->attractionThreshold());
    //QObject::connect(m_attractionThresholdSlider, SIGNAL(valueChanged(int)), informationRegulator, SLOT(onSetThreshold(int)));
	//
    //vBoxLay->addLayout(hlay);
    //vBoxLay->addWidget(infoLabel, 0, Qt::AlignCenter);
    //vBoxLay->addWidget(m_attractionThresholdSlider, 0, Qt::AlignCenter);
	//
	//
    //wid->show();


    return a.exec();
}
