#include "EyeFeature.h"


EyeFeature::EyeFeature(const cv::Vec6d & PosTransAndRotation, const cv::Point3f & gaze1, const cv::Point3f & gaze2)
{
	for (auto i = 0; i < 6; ++i) {
		m_feature.push_back(PosTransAndRotation[i]);
	}
	m_feature.push_back(gaze1.x);
	m_feature.push_back(gaze1.y);
	m_feature.push_back(gaze1.z);
	m_feature.push_back(gaze2.x);
	m_feature.push_back(gaze2.y);
	m_feature.push_back(gaze2.z);
}

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
	size_t pos = txt.find(ch);
	size_t initialPos = 0;
	strs.clear();

	while (pos != std::string::npos) {
		strs.push_back(txt.substr(initialPos, pos - initialPos));
		initialPos = pos + 1;

		pos = txt.find(ch, initialPos);
	}

	strs.push_back(txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1));

	return strs.size();
}
EyeFeature::EyeFeature(const std::string & feature)
{
	std::vector<std::string> strs;
	split(feature, strs, ' ');
	for (auto const & s : strs) {
		if (s.empty()) {
			continue;
		}
		m_feature.push_back(std::stod(s));
	}
}

EyeFeature::EyeFeature()
{
	m_id = -1;
}

void EyeFeature::setId(int id)
{
	m_id = id;
}

int EyeFeature::getId() const
{
	return m_id;
}

void EyeFeature::normalaizeFeature(const std::vector<double>& vec)
{
	double distance = 0;
	size_t size = m_feature.size() < vec.size() ? m_feature.size() : vec.size();
	for (auto i = 0; i < size; ++i) {
		m_feature.at(i) /= vec.at(i);
	}
}

double EyeFeature::distance(const EyeFeature & eyeFeature) const
{
	double distance = 0;
	size_t size = m_feature.size() < eyeFeature.m_feature.size() ? m_feature.size() : eyeFeature.m_feature.size();
	for (auto i = 0; i < size; ++i) {
		distance += (m_feature.at(i) - eyeFeature.m_feature.at(i))*(m_feature.at(i) - eyeFeature.m_feature.at(i));
	}
	return distance;
}

std::vector<double> EyeFeature::getFeature() const
{
	return m_feature;
}

EyeFeature::operator std::string()
{
	std::string str;
	for (auto i = 0; i < m_feature.size(); ++i) {
		str += std::to_string(m_feature.at(i));
		if (i != m_feature.size() - 1) {
			str += " ";
		}
	}
	return str;
}

bool EyeFeature::operator<(const EyeFeature & other) const
{
	return m_id < other.m_id;
	int size = m_feature.size() > other.m_feature.size() ? other.m_feature.size() : m_feature.size();
	for (auto i = 0; i < size; ++i) {
		if (m_feature.at(i) < other.m_feature.at(i)) {
			return true;
		}
		else if (m_feature.at(i) == other.m_feature.at(i))
		{
			continue;
		}
		else {
			return false;
		}
	}
	return false;
}


std::ostream & operator<<(std::ostream & out, const EyeFeature & eF)
{
	for (auto const & f : eF.getFeature()) {
		out << f << " ";
	}
	out << "\n";
}
