#pragma once

#include <QThread>
#include <QTimer>

#include "EyeFeature.h"
#include <mutex>
class EyeTrackingReporter : public QObject
{
	Q_OBJECT
public:
	EyeTrackingReporter(int sec , QThread* parent = nullptr);
public slots:
	void start();
	void stop();
	void onTimeout();
	void setFeature(const EyeFeature& eyeFeature);
	void setConfidence(double confidence);
signals:
	void detectScreen();
private:
	double m_confidence;
	EyeFeature m_feature;
	int m_sec;
	QTimer* m_timer; 
	QThread* m_thread;
	std::mutex m_mutex;
};